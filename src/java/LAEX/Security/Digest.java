/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Security;

import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Defines method of secure hashing for salt and password
 * @author Lech Jankowski
 */

public class Digest
{

	/**
	 * Converts byte representation of a string to hexadecimal representation
	 * @param b
	 * @return hex string
	 */
	public static String bytesToHex(byte[] b) {
       char hexDigit[] = {'0', '1', '2', '3', '4', '5', '6', '7',
                          '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
       StringBuffer buf = new StringBuffer();
       for (int j=0; j<b.length; j++) {
          buf.append(hexDigit[(b[j] >>> 4) & 0x0f]);
          buf.append(hexDigit[b[j] & 0x0f]);
       }
       return buf.toString();
    }        
    
	/**
	 * Generates secure random salt
	 * @return hex string of a salt
	 */
	public static String generateHexSalt() {
        Random sr = new SecureRandom();
        byte[] salt = new byte[32];
        sr.nextBytes(salt);
        return bytesToHex(salt);
    }    

	/**
	 * Hashes given string with SHA2-256 algorithm
	 * @param input
	 * @return 256 bit hashed string (hex representation)
	 */
	public static String stringToHashSHA256(String input) {
         String hexSHA256 = null;
         try {      
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(input.getBytes()); 
            byte[] output = md.digest();
            hexSHA256 = bytesToHex(output);                
         } catch (Exception e) {
            System.out.println("Exception: "+e);
         }
         return hexSHA256;
    }
	
	/**
	 * Tests
	 * @param args
	 */
	public static void main(String[] args)
    {
		String mySalt = generateHexSalt();
		System.out.println("mySalt:                                        : " + mySalt);
		
		String myPasswordNoSalt = stringToHashSHA256("password1");
		System.out.println("myPasswordNoSalt:                      [password1]: " + myPasswordNoSalt);
		
		String myPasswordWithSalt = stringToHashSHA256(mySalt+"password1");
		System.out.println("myPasswordWithSalt:             [mySalt+password1]: " + myPasswordWithSalt);
	}
	
}


/*
MessageDigest digest = MessageDigest.getInstance("SHA-256");
byte[] hash = digest.digest(text.getBytes("UTF-8"));
*/
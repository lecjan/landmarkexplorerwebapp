/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Business;

import java.util.Objects;

/**
 * Tag used to categorise and link information together e.g. nodes or ads to tag="hiking"
 * @author Lech Jankowski
 */
public class Tag {
	
	private int tagID;
	private String name;

	/**
	 * 
	 * @param tagID unique tag identificator
	 * @param name tag name
	 */
	public Tag(int tagID, String name) {
		this.tagID = tagID;
		this.name = name;
	}

	/**
	 * 
	 * @return unique id
	 */
	public int getTagID() {
		return tagID;
	}

	/**
	 *
	 * @param tagID
	 */
	public void setTagID(int tagID) {
		this.tagID = tagID;
	}

	/**
	 *
	 * @return tag name
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 97 * hash + this.tagID;
		hash = 97 * hash + Objects.hashCode(this.name);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Tag other = (Tag) obj;
		if (this.tagID != other.tagID) {
			return false;
		}
		if (!Objects.equals(this.name, other.name)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Tag{" + "tagID=" + tagID + ", name=" + name + '}';
	}
	
	
	
}

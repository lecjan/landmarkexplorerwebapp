/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Business;

import java.util.Objects;
import java.util.logging.Logger;

/**
 * Credentials for User for a specific application.
 * Note!! userID and passWord can be same for different application.
 * This class identifies a user having the right to use application and its credentials
 * This class reflect relation from D00161848_landmark database [user_apps]
 * @author Lech Jankowski
 */
public class UserApplication {
    
    private String userID;
	private String passWord;
    private String appSymbol;

	/**
	 *
	 */
	public UserApplication() {
        this.userID = null;
		this.passWord = null;
        this.appSymbol = null;
    }    

	/**
	 * 
	 * @param userID unique user id (hashed with secure random)
	 * @param passWord user password hashed using salt=userID and plain user password
	 * @param appSymbol symbol of an application (see Application class)
	 */
	public UserApplication(String userID, String passWord, String appSymbol) {
        this.userID = userID;
		this.passWord = passWord;
		this.appSymbol = appSymbol;
    }

	/**
	 *
	 * @return unique user id
	 */
	public String getUserID() {
        return userID;
    }

	/**
	 *
	 * @return password
	 */
	public String getPassword() {
		return passWord;
	}

	/**
	 *
	 * @param passWord
	 */
	public void setPassword(String passWord) {
		this.passWord = passWord;
	}

	/**
	 *
	 * @param userID user id
	 */
	public void setUserID(String userID) {
        this.userID = userID;
    }

	/**
	 *
	 * @return application symbol
	 */
	public String getAppSymbol() {
        return appSymbol;
    }

	/**
	 *
	 * @param appSymbol
	 */
	public void setAppSymbol(String appSymbol) {
        this.appSymbol = appSymbol;
    }


	@Override
	public int hashCode() {
		int hash = 5;
		hash = 17 * hash + Objects.hashCode(this.userID);
		hash = 17 * hash + Objects.hashCode(this.passWord);
		hash = 17 * hash + Objects.hashCode(this.appSymbol);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final UserApplication other = (UserApplication) obj;
		if (!Objects.equals(this.userID, other.userID)) {
			return false;
		}
		if (!Objects.equals(this.passWord, other.passWord)) {
			return false;
		}
		if (!Objects.equals(this.appSymbol, other.appSymbol)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "UserApplication{" + "userID=" + userID + ", passWord=" + passWord + ", appSymbol=" + appSymbol + '}';
	}
	

    
    
    
}

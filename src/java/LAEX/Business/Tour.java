/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Business;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Objects;

/**
 * Tour defines group of landmark nodes.points
 * @author Lech Jankowski
 */
public class Tour {
	
	private int tourID;
	private String name;
	private String description;
	private String country;
	private String region;
	private int startPoint;
	private int[] multiPoints;

	/**
	 * 
	 * @param tourID unique tour id
	 * @param name short tour name
	 * @param description full tour description
	 * @param country country of tour location
	 * @param region region of tour location
	 * @param startPoint id of node/point where tour starts
	 * @param multiPoints array of nodes belonging to tour (absolete for now) to use Neo4j grouping
	 */
	public Tour(int tourID, String name, String description, String country, String region, int startPoint, int[] multiPoints) {
		this.tourID = tourID;
		this.name = name;
		this.description = description;
		this.country = country;
		this.region = region;
		this.startPoint = startPoint;
		this.multiPoints = multiPoints;
	}

	/**
	 *
	 * @return unique tour id
	 */
	public int getTourID() {
		return tourID;
	}

	/**
	 *
	 * @param tourID
	 */
	public void setTourID(int tourID) {
		this.tourID = tourID;
	}

	/**
	 *
	 * @return tour name
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 *
	 * @return tour description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 *
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 *
	 * @return country of a tour
	 */
	public String getCountry() {
		return country;
	}

	/**
	 *
	 * @param country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 *
	 * @return region of a tour
	 */
	public String getRegion() {
		return region;
	}

	/**
	 *
	 * @param region
	 */
	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 *
	 * @return node/point id where tour starts
	 */
	public int getStartPoint() {
		return startPoint;
	}

	/**
	 *
	 * @param startPoint
	 */
	public void setStartPoint(int startPoint) {
		this.startPoint = startPoint;
	}

	/**
	 *
	 * @return array of node/point ids
	 */
	public int[] getMultiPoints() {
		return multiPoints;
	}

	/**
	 *
	 * @param multiPoints
	 */
	public void setMultiPoints(int[] multiPoints) {
		this.multiPoints = multiPoints;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 97 * hash + this.tourID;
		hash = 97 * hash + Objects.hashCode(this.name);
		hash = 97 * hash + Objects.hashCode(this.description);
		hash = 97 * hash + Objects.hashCode(this.country);
		hash = 97 * hash + Objects.hashCode(this.region);
		hash = 97 * hash + this.startPoint;
		hash = 97 * hash + Arrays.hashCode(this.multiPoints);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Tour other = (Tour) obj;
		if (this.tourID != other.tourID) {
			return false;
		}
		if (!Objects.equals(this.name, other.name)) {
			return false;
		}
		if (!Objects.equals(this.description, other.description)) {
			return false;
		}
		if (!Objects.equals(this.country, other.country)) {
			return false;
		}
		if (!Objects.equals(this.region, other.region)) {
			return false;
		}
		if (this.startPoint != other.startPoint) {
			return false;
		}
		if (!Arrays.equals(this.multiPoints, other.multiPoints)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Tour{" + "tourID=" + tourID + ", name=" + name + ", description=" + description + ", country=" + country + ", region=" + region + ", startPoint=" + startPoint + ", multiPoints=" + Arrays.toString(multiPoints) + '}';
	}
	
	
			
}

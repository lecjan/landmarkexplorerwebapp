/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Business;

import LAEX.Business.CustomDataType.GeoPoint;
import LAEX.Business.CustomDataType.PointType;
import java.util.Objects;

/**
 *  Defines a node MapPoint class for a graph database.
 * Also represents a pinpoint on the map
 * @author Lech Jankowski
 */
public class MapPoint {
	
	private int PointID;
	private String name;
	private String description;
	private PointType type;
	private GeoPoint geoLocation;

	/**
	 *
	 */
	public MapPoint() {
		this.PointID = -1;
		this.name = "";
		this.description = "";
		this.type = PointType.DEFAULT;
		this.geoLocation = new GeoPoint(0,0);
	}	
	
	/**
	 * 
	 * @param PointID	unique point/node identificator		
	 * @param name	short name for a point/node
	 * @param description	full description of a point/node
	 * @param type	type of a node/point of type PointType
	 * @param geoLocation geo location paramemeters (latitude,longitude) of type GeoPoint
	 */
	public MapPoint(int PointID, String name, String description, PointType type, GeoPoint geoLocation) {
		this.PointID = PointID;
		this.name = name;
		this.description = description;
		this.type = type;
		this.geoLocation = geoLocation;
	}

	// copy constructor

	/**
	 * copy constructor
	 * @param aPoint
	 * @return
	 */
		public static MapPoint newInstance(MapPoint aPoint) {
		return new MapPoint(aPoint.getPointID(), aPoint.getName(), aPoint.getDescription(), aPoint.getType(), aPoint.getGeoLocation());
	}	

	/**
	 * 
	 * @return unique point/node id
	 */
	public int getPointID() {
		return PointID;
	}

	/**
	 *
	 * @param PointID
	 */
	public void setPointID(int PointID) {
		this.PointID = PointID;
	}

	/**
	 * 
	 * @return point/node short name
	 */
	public String getName() {
		return name;
	}

	/**
	 *
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return point/node full description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 *
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 *
	 * @return point/node type
	 */
	public PointType getType() {
		return type;
	}

	/**
	 *
	 * @param type
	 */
	public void setType(PointType type) {
		this.type = type;
	}

	/**
	 *
	 * @return geo location
	 */
	public GeoPoint getGeoLocation() {
		return geoLocation;
	}

	/**
	 *
	 * @param geoLocation
	 */
	public void setGeoLocation(GeoPoint geoLocation) {
		this.geoLocation = geoLocation;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 19 * hash + this.PointID;
		hash = 19 * hash + Objects.hashCode(this.name);
		hash = 19 * hash + Objects.hashCode(this.description);
		hash = 19 * hash + Objects.hashCode(this.type);
		hash = 19 * hash + Objects.hashCode(this.geoLocation);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final MapPoint other = (MapPoint) obj;
		if (this.PointID != other.PointID) {
			return false;
		}
		if (!Objects.equals(this.name, other.name)) {
			return false;
		}
		if (!Objects.equals(this.description, other.description)) {
			return false;
		}
		if (this.type != other.type) {
			return false;
		}
		if (!Objects.equals(this.geoLocation, other.geoLocation)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Point{" + "PointID=" + PointID + ", name=" + name + ", description=" + description + ", type=" + type + ", geoLocation=" + geoLocation + '}';
	}
	
	
}


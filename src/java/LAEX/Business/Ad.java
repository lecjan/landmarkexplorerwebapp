/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Business;

import java.util.Objects;

/**
 * Commercial advertisement used to appear on users dashboard
 * @author Lech Jankowski
 */
public class Ad {
	
	private int adID;
	private String extUrl;
	private String imgUrl;
	private String text;

	/**
	 * 
	 * @param adID unique ad identificator
	 * @param extUrl url link to external commercial page
	 * @param imgUrl url link to image (maybe on LAEX server)
	 * @param text ad text to appear on dashboard
	 */
	public Ad(int adID, String extUrl, String imgUrl, String text) {
		this.adID = adID;
		this.extUrl = extUrl;
		this.imgUrl = imgUrl;		
		this.text = text;
	}

	/**
	 *
	 * @return ad identificator
	 */
	public int getAdID() {
		return adID;
	}

	/**
	 *
	 * @param adID
	 */
	public void setAdID(int adID) {
		this.adID = adID;
	}

	/**
	 *
	 * @return external ad URL
	 */
	public String getExtUrl() {
		return extUrl;
	}

	/**
	 *
	 * @param extUrl
	 */
	public void setExtUrl(String extUrl) {
		this.extUrl = extUrl;
	}

	/**
	 *
	 * @return image URL
	 */
	public String getImgUrl() {
		return imgUrl;
	}

	/**
	 *
	 * @param imgUrl
	 */
	public void setImgUrl(String imgUrl) {
		this.imgUrl = imgUrl;
	}

	/**
	 *
	 * @return ad text
	 */
	public String getText() {
		return text;
	}

	/**
	 *
	 * @param text
	 */
	public void setText(String text) {
		this.text = text;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 89 * hash + this.adID;
		hash = 89 * hash + Objects.hashCode(this.extUrl);
		hash = 89 * hash + Objects.hashCode(this.imgUrl);
		hash = 89 * hash + Objects.hashCode(this.text);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Ad other = (Ad) obj;
		if (this.adID != other.adID) {
			return false;
		}
		if (!Objects.equals(this.extUrl, other.extUrl)) {
			return false;
		}
		if (!Objects.equals(this.imgUrl, other.imgUrl)) {
			return false;
		}
		if (!Objects.equals(this.text, other.text)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Ad{" + "adID=" + adID + ", extUrl=" + extUrl + ", imgUrl=" + imgUrl + ", text=" + text + '}';
	}
	
	
	
}

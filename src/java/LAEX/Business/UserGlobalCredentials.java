/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Business;

import java.util.Arrays;
import java.util.Objects;

/**
 * Global Credential of a user common to any application within organisation.
 * This class reflects relation in d00161848_landmark [global_credentials]
 * @author Lech Jankowski
 */
public class UserGlobalCredentials {
    
    private String userID;
    private String userName;
    
    private String firstName;
    private String lastName;
	
    private String addrLine1;
    private String addrLine2;
    private String addrLine3;
    private String addrCity;
    private String addrRegion;
    private String addrCountry;
    private String contactPhone;
    private boolean contactPhoneConcent;
    private String contactEmail;
    private boolean contactEmailConcent;

	/**
	 *
	 */
	public UserGlobalCredentials() {
	}	

	/**
	 * 
	 * @param userID unique user id (secure random hash)
	 * @param userName username
	 * @param firstName user's first name
	 * @param lastName user's last name
	 * @param addrLine1 user's address line 1
	 * @param addrLine2 user's address line 2
	 * @param addrLine3 user's address line 3
	 * @param addrCity user's address city
	 * @param addrRegion user's address region/county
	 * @param addrCountry user's address country
	 * @param contactPhone user's contact phone no
	 * @param contactPhoneConcent user's consent to contact by phone
	 * @param contactEmail user's email address
	 * @param contactEmailConcent user's consent to contact by email
	 */
	public UserGlobalCredentials(String userID, String userName, String firstName, String lastName, String addrLine1, String addrLine2, String addrLine3, String addrCity, String addrRegion, String addrCountry, String contactPhone, boolean contactPhoneConcent, String contactEmail, boolean contactEmailConcent) {
        this.userID = userID;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
		this.addrLine1 = addrLine1;
        this.addrLine2 = addrLine2;
        this.addrLine3 = addrLine3;
        this.addrCity = addrCity;
        this.addrRegion = addrRegion;
        this.addrCountry = addrCountry;
        this.contactPhone = contactPhone;
        this.contactPhoneConcent = contactPhoneConcent;
        this.contactEmail = contactEmail;
    }

	/**
	 *
	 * @return unique user id
	 */
	public String getUserID() {
        return userID;
    }

	/**
	 *
	 * @param userID
	 */
	public void setUserID(String userID) {
        this.userID = userID;
    }

	/**
	 *
	 * @return username
	 */
	public String getUserName() {
        return userName;
    }

	/**
	 *
	 * @param userName
	 */
	public void setUserName(String userName) {
        this.userName = userName;
    }

	/**
	 *
	 * @return user's first name
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 *
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 *
	 * @return user's last name
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 *
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 *
	 * @return user's address line 1
	 */
	public String getAddrLine1() {
        return addrLine1;
    }

	/**
	 *
	 * @param addrLine1
	 */
	public void setAddrLine1(String addrLine1) {
        this.addrLine1 = addrLine1;
    }

	/**
	 *
	 * @return user's address line 2
	 */
	public String getAddrLine2() {
        return addrLine2;
    }

	/**
	 *
	 * @param addrLine2
	 */
	public void setAddrLine2(String addrLine2) {
        this.addrLine2 = addrLine2;
    }

	/**
	 *
	 * @return user's address line 3
	 */
	public String getAddrLine3() {
        return addrLine3;
    }

	/**
	 *
	 * @param addrLine3
	 */
	public void setAddrLine3(String addrLine3) {
        this.addrLine3 = addrLine3;
    }

	/**
	 *
	 * @return user's address city
	 */
	public String getAddrCity() {
        return addrCity;
    }

	/**
	 *
	 * @param addrCity
	 */
	public void setAddrCity(String addrCity) {
        this.addrCity = addrCity;
    }

	/**
	 *
	 * @return user's address region
	 */
	public String getAddrRegion() {
        return addrRegion;
    }

	/**
	 *
	 * @param addrRegion
	 */
	public void setAddrRegion(String addrRegion) {
        this.addrRegion = addrRegion;
    }

	/**
	 *
	 * @return user's address country
	 */
	public String getAddrCountry() {
        return addrCountry;
    }

	/**
	 *
	 * @param addrCountry
	 */
	public void setAddrCountry(String addrCountry) {
        this.addrCountry = addrCountry;
    }

	/**
	 *
	 * @return user's phone number
	 */
	public String getContactPhone() {
        return contactPhone;
    }

	/**
	 *
	 * @param contactPhone
	 */
	public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

	/**
	 *
	 * @return phone contact consent is true or false 
	 */
	public boolean isContactPhoneConcent() {
        return contactPhoneConcent;
    }

	/**
	 *
	 * @param contactPhoneConcent
	 */
	public void setContactPhoneConcent(boolean contactPhoneConcent) {
        this.contactPhoneConcent = contactPhoneConcent;
    }

	/**
	 *
	 * @return user's email address
	 */
	public String getContactEmail() {
        return contactEmail;
    }

	/**
	 *
	 * @param contactEmail
	 */
	public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

	/**
	 *
	 * @return email contact consent is true of false
	 */
	public boolean isContactEmailConcent() {
        return contactEmailConcent;
    }

	/**
	 *
	 * @param contactEmailConcent
	 */
	public void setContactEmailConcent(boolean contactEmailConcent) {
        this.contactEmailConcent = contactEmailConcent;
    }

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 71 * hash + Objects.hashCode(this.userID);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final UserGlobalCredentials other = (UserGlobalCredentials) obj;
		if (!Objects.equals(this.userID, other.userID)) {
			return false;
		}
		return true;
	}

    @Override
    public String toString() {
        return "UserGlobalCredentials{" + "userID=" + userID + ", userName=" + userName + ", firstName=" + firstName + ", lastName=" + lastName + ", addrLine1=" + addrLine1 + ", addrLine2=" + addrLine2 + ", addrLine3=" + addrLine3 + ", addrCity=" + addrCity + ", addrRegion=" + addrRegion + ", addrCountry=" + addrCountry + ", contactPhone=" + contactPhone + ", contactPhoneConcent=" + contactPhoneConcent + ", contactEmail=" + contactEmail + ", contactEmailConcent=" + contactEmailConcent + '}';
    }
    
}

/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Business.User;

/**
 * Interface forcing to define/implement specific methods for users regarding privileges
 * @author Lech Jankowski
 */
public interface Privilege {
	
	/**
	 *
	 * @return
	 */
	public boolean addUsers();

	/**
	 *
	 * @return
	 */
	public boolean readUsers();	

	/**
	 *
	 * @return
	 */
	public boolean deleteUsers();

	/**
	 *
	 * @return
	 */
	public boolean updateUsers();
	
	/**
	 *
	 * @return
	 */
	public boolean addPinPoints();

	/**
	 *
	 * @return
	 */
	public boolean readPinPoints();

	/**
	 *
	 * @return
	 */
	public boolean deletePinPoints();

	/**
	 *
	 * @return
	 */
	public boolean updatePinPoints();
	
	/**
	 *
	 * @return
	 */
	public boolean addAds();

	/**
	 *
	 * @return
	 */
	public boolean readAds();

	/**
	 *
	 * @return
	 */
	public boolean deleteAds();

	/**
	 *
	 * @return
	 */
	public boolean updateAds();
	
}

/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Business.User;

import LAEX.Business.CustomDataType.AccountPrivilege;
import LAEX.Business.CustomDataType.AccountType;
import LAEX.Business.CustomDataType.AccountTypePrivilege;
import java.util.Objects;

/**
 * Superclass for application users active in application
 * @author Lech Jankowski
 */
public abstract class User {
	
    private String userID;
    private String userName;
    private AccountType type = AccountType.NOT_SET;  
	private AccountPrivilege privilege = AccountPrivilege.NOT_SET;
	private boolean loggedIn;	

	/**
	 *
	 * @param userID unique user id (secret random hash)
	 * @param userName username
	 * @param type user account type (private,commercial,administrator)
	 * @param privilege user privilege type (basic,premium,supervisor)
	 * @param online online status (is logged in CURRENT or is connected online TBC)
	 */
	public User(String userID, String userName, AccountType type, AccountPrivilege privilege, boolean online) {
		this.userID = userID;
		this.userName = userName;
		this.type = type;
		this.privilege = privilege;
		this.loggedIn = online;		
	}	

	/**
	 *
	 * @param userID unique user id
	 * @param userName username
	 * @param combiParam account type of used by AccountTypePrivilege [TBC]
	 * @param online TBC
	 */
	public User(String userID, String userName, byte combiParam, boolean online) {
		this.userID = userID;
		this.userName = userName;
		
		/* 
		converting and splitting combined account parameters to enum types of AccountType and AccountPrivilege
		*/
		this.type = new AccountTypePrivilege(combiParam).getTypeEnum();
		this.privilege = new AccountTypePrivilege(combiParam).getPrivilegeEnum();
		
		this.loggedIn = online;		
	}	
	
	/**
	 *
	 * @param userID unique user id
	 * @param userName username
	 */
	public User(String userID, String userName) {
		this.userID = userID;
		this.userName = userName;
		this.loggedIn = false;		
	}		
	
	/**
	 * TBC
	 */
	protected abstract void construct(); 

	/**
	 *
	 * @return unique user id
	 */
	public String getUserID() {
		return userID;
	}

	/**
	 *
	 * @param userID
	 */
	public void setUserID(String userID) {
		this.userID = userID;
	}

	/**
	 *
	 * @return username
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 *
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 *
	 * @return account type enum
	 */
	public AccountType getType() {
		return type;
	}
	
	/**
	 *
	 * @return account type byte
	 */
	public byte getTypeByte() {
		switch (this.type)
		{
			case PRIVATE:
				return (byte) 1;
			case COMMERCIAL:
				return (byte) 2;
			case ADMINISTRATOR:
				return (byte) 3;
			default:
				return (byte) 0;
		}		
	}	

	/**
	 *
	 * @param type
	 */
	public void setType(AccountType type) {
		this.type = type;
	}

	/**
	 *
	 * @return account privilege enum
	 */
	public AccountPrivilege getPrivilege() {
		return privilege;
	}

	/**
	 *
	 * @return account privilege byte
	 */
	public byte getPrivilegeByte() {
		switch (this.privilege)
		{
			case BASIC:
				return (byte) 1;
			case PREMIUM:
				return (byte) 2;
			case SUPERVISOR:
				return (byte) 3;
			default:
				return (byte) 0;
		}
	}	
	
	/**
	 *
	 * @param privilege
	 */
	public void setPrivilege(AccountPrivilege privilege) {
		this.privilege = privilege;
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 29 * hash + Objects.hashCode(this.userID);
		return hash;
	}
	
	/**
	 *
	 * @return
	 */
	public boolean isLoggedIn() {
		return loggedIn;
	}

	/**
	 *
	 * @param loggedIn
	 */
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}	

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final User other = (User) obj;
		if (!Objects.equals(this.userID, other.userID)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "UserAccount{" + "userID=" + userID + ", userName=" + userName + ", type=" + type + ", privilege=" + privilege + ", loggedIn=" + loggedIn + '}';
	}





	



	

}

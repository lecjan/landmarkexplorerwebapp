/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Business.User;

import LAEX.Business.CustomDataType.AccountPrivilege;
import LAEX.Business.CustomDataType.AccountType;
import LAEX.Business.CustomDataType.GeoPoint;

/**
 * Sub-class of a user - standard "vendor" user with basic privileges
 * @author Lech Jankowski
 */
public class UserCommercialBasic extends User implements Privilege {
    
    private GeoPoint geoResidence;	 
	
	/**
	 *
	 * @param userID unique user id (hash)
	 * @param userName username
	 * @param geoR geographic location of residence
	 * @param online TBC
	 */
	public UserCommercialBasic(String userID, String userName, GeoPoint geoR, boolean online) {
        super(userID, userName, AccountType.COMMERCIAL, AccountPrivilege.BASIC, online);
		this.geoResidence = geoR;
		construct();
    }
	
	/**
	 *
	 * @return geographical point of residence
	 */
	public GeoPoint getGeoResidence() {
		return geoResidence;
	}

	/**
	 *
	 * @param geoResidence
	 */
	public void setGeoResidence(GeoPoint geoResidence) {
		this.geoResidence = geoResidence;
	}		
	
	/**
	 *
	 */
	@Override
	protected void construct()
	{
		System.out.println("Creating commercial basic account");
	}
	
	/**
	 *
	 * @return false
	 */
	@Override
	public boolean addUsers() {
		System.out.println("User can't add other users account");
		return false;
	}
	
	/**
	 *
	 * @return false
	 */
	@Override
	public boolean readUsers() {
		System.out.println("User can't read other users account");
		return false;
	}
	
	/**
	 *
	 * @return false
	 */
	@Override
	public boolean deleteUsers() {
		System.out.println("User can't delete other users account");
		return false;
	}
	
	/**
	 *
	 * @return false
	 */
	@Override
	public boolean updateUsers() {
		System.out.println("User can't update other users account");
		return false;
	}
	
	/**
	 *
	 * @return
	 */
	@Override
	public boolean addPinPoints() {
		System.out.println("User can add places !!!");
		return true;
	}			

	/**
	 *
	 * @return true
	 */
	@Override
	public boolean readPinPoints() {
		System.out.println("User can see places !!!"); 
		return true;
	}			
	
	/**
	 *
	 * @return true
	 */
	@Override
	public boolean deletePinPoints() {
		System.out.println("User can delete places !!!");
		return true;
	}			
	
	/**
	 *
	 * @return true
	 */
	@Override
	public boolean updatePinPoints() {
		System.out.println("User can update places !!!");
		return true;
	}			
	
	/**
	 *
	 * @return
	 */
	@Override
	public boolean addAds() {
		System.out.println("User can't add adverts");
		return false;
	}			
	
	/**
	 *
	 * @return true
	 */
	@Override
	public boolean readAds() {
		System.out.println("User can see adverts !!!");
		return true;
	}			
	
	/**
	 *
	 * @return false
	 */
	@Override
	public boolean deleteAds() {
		System.out.println("User can't delete adverts");
		return false;
	}			
	
	/**
	 *
	 * @return false
	 */
	@Override
	public boolean updateAds() {
		System.out.println("User can't update adverts");
		return false;
	}		
	
	@Override
	public String toString() {
		return "UserCommercialBasic{" + super.toString() + ", geoResidence=" + geoResidence + '}';
	}	
	
    // Class tests

	/**
	 *
	 * @param args
	 */
	    public static void main(String[] args)
    {
		UserCommercialBasic user = new UserCommercialBasic("12345678901234567890123456789012","lecjan", null, false);
		System.out.println(user.toString());
		
		user.addUsers();
		user.readUsers();
		user.deleteUsers();
		user.updateUsers();
		
		user.addPinPoints();
		user.readPinPoints();
		user.deletePinPoints();
		user.updatePinPoints();
		
		user.addAds();
		user.readAds();
		user.deleteAds();
		user.updateAds();		
    }		

}

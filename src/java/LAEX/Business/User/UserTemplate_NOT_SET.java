/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Business.User;

import LAEX.Business.CustomDataType.AccountPrivilege;
import LAEX.Business.CustomDataType.AccountType;

/**
 * Class used when incoming user acc type and privilege is NOT_SET. For debugging
 * @author Lech Jankowski
 */
public class UserTemplate_NOT_SET extends User implements Privilege {
    
	/**
	 *
	 * @param userID
	 * @param userName
	 * @param online
	 */
	public UserTemplate_NOT_SET(String userID, String userName, boolean online) {
        super(userID, userName, AccountType.NOT_SET, AccountPrivilege.NOT_SET, online);
		construct();
    }
	
	/**
	 *
	 */
	@Override
	protected void construct()
	{
		System.out.println("Creating template account for user with NOT_SET type and privilege");
	}
	
	/**
	 *
	 * @return
	 */
	@Override
	public boolean addUsers() {
		System.out.println("User can't add other users account");
		return false;
	}
	
	/**
	 *
	 * @return
	 */
	@Override
	public boolean readUsers() {
		System.out.println("User can't read other users account");
		return false;
	}
	
	/**
	 *
	 * @return
	 */
	@Override
	public boolean deleteUsers() {
		System.out.println("User can't delete other users account");
		return false;
	}
	
	/**
	 *
	 * @return
	 */
	@Override
	public boolean updateUsers() {
		System.out.println("User can't update other users account");
		return false;
	}
	
	/**
	 *
	 * @return
	 */
	@Override
	public boolean addPinPoints() {
		System.out.println("User can't add places");
		return false;
	}			

	/**
	 *
	 * @return
	 */
	@Override
	public boolean readPinPoints() {
		System.out.println("User can't see places"); 
		return false;
	}			
	
	/**
	 *
	 * @return
	 */
	@Override
	public boolean deletePinPoints() {
		System.out.println("User can't delete places");
		return false;
	}			
	
	/**
	 *
	 * @return
	 */
	@Override
	public boolean updatePinPoints() {
		System.out.println("User can't update places");
		return false;
	}			
	
	/**
	 *
	 * @return
	 */
	@Override
	public boolean addAds() {
		System.out.println("User can't add adverts");
		return false;
	}			
	
	/**
	 *
	 * @return
	 */
	@Override
	public boolean readAds() {
		System.out.println("User can't see adverts");
		return false;
	}			
	
	/**
	 *
	 * @return
	 */
	@Override
	public boolean deleteAds() {
		System.out.println("User can't delete adverts");
		return false;
	}			

	/**
	 *
	 * @return
	 */
	@Override
	public boolean updateAds() {
		System.out.println("User can't update adverts");
		return false;
	}			
	
	@Override
	public String toString() {
		return "UserTemplate_NOT_SET{" + super.toString() + "}";
	}
		
    // Class tests

	/**
	 *
	 * @param args
	 */
	    public static void main(String[] args)
    {
		UserTemplate_NOT_SET user = new UserTemplate_NOT_SET("12345678901234567890123456789012","lecjan",false);
		System.out.println(user.toString());
		
		user.addUsers();
		user.readUsers();
		user.deleteUsers();
		user.updateUsers();
		
		user.addPinPoints();
		user.readPinPoints();
		user.deletePinPoints();
		user.updatePinPoints();
		
		user.addAds();
		user.readAds();
		user.deleteAds();
		user.updateAds();		
    }	

}

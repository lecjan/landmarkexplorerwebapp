/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Business;

import java.util.Arrays;
import java.util.Objects;

/**
 * Class defining system application codes used to user access control.
 * Organisation may have various applications and same user can have awarded access to fewer applications.
 * Therefore they must be defined and identified.
 * @author Lech Jankowski
 */
public class Application {
    
    private String appSymbol;
    private String appName;
    private byte[] appKey;

	/**
	 *
	 */
	public Application() {
        this.appSymbol = null;
        this.appName = null;
        this.appKey = null;
    }
    
	/**
	 * 
	 * @param appSymbol symbol used to identify application
	 * @param appName full name of application
	 * @param appKey hashed key used for validation of an application use
	 */
	public Application(String appSymbol, String appName, byte[] appKey) {
        this.appSymbol = appSymbol;
        this.appName = appName;
        this.appKey = appKey;
    }

	/**
	 * 
	 * @return application symbol
	 */
	public String getAppSymbol() {
        return appSymbol;
    }

	/**
	 *
	 * @param appSymbol 
	 */
	public void setAppSymbol(String appSymbol) {
        this.appSymbol = appSymbol;
    }

	/**
	 * 
	 * @return application full name
	 */
	public String getAppName() {
        return appName;
    }

	/**
	 *
	 * @param appName
	 */
	public void setAppName(String appName) {
        this.appName = appName;
    }

	/**
	 * 
	 * @return application hashed key
	 */
	public byte[] getAppKey() {
        return appKey;
    }

	/**
	 *
	 * @param appKey
	 */
	public void setAppKey(byte[] appKey) {
        this.appKey = appKey;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 53 * hash + Objects.hashCode(this.appSymbol);
        hash = 53 * hash + Objects.hashCode(this.appName);
        hash = 53 * hash + Arrays.hashCode(this.appKey);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Application other = (Application) obj;
        if (!Objects.equals(this.appSymbol, other.appSymbol)) {
            return false;
        }
        if (!Objects.equals(this.appName, other.appName)) {
            return false;
        }
        if (!Arrays.equals(this.appKey, other.appKey)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Application{" + "appSymbol=" + appSymbol + ", appName=" + appName + ", appKey=" + Arrays.toString(appKey) + '}';
    }
    

    
}

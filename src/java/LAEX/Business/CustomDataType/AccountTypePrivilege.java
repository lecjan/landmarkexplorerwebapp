/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Business.CustomDataType;

import LAEX.Exceptions.AccountTypePrivilegeException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class defines combined numeric representation of an account type and privilege of an user.
 * This mirrors data stored in d00161848_landmark database relation [app_user].
 * Class contains method to convert to and from specific data types and classes [see types AccountProvilege, AccountType]
 * @author Lech Jankowski
 */
public class AccountTypePrivilege
{

    private static final byte PRIVILEGE_NULL = 0;
    private static final byte PRIVILEGE_BASIC = 1;
    private static final byte PRIVILEGE_PREMIUM = 2;
    private static final byte PRIVILEGE_SUPERVISOR = 3;
    private static final byte PRIVILEGE_LEVEL_MIN = 1;
    private static final byte PRIVILEGE_LEVEL_MAX = 3;

    private static final byte TYPE_NULL = 0;
    private static final byte TYPE_ADMIN = 3;
    private static final byte TYPE_COMMERCIAL = 2;
    private static final byte TYPE_PRIVATE = 1;
    private static final byte TYPE_MIN = 1;
    private static final byte TYPE_MAX = 3;

    private byte privilege = PRIVILEGE_NULL;
    private byte type = TYPE_NULL;

	/**
	 * default constructor
	 */
	public AccountTypePrivilege()
    {
	this.privilege = PRIVILEGE_NULL;
	this.type = TYPE_NULL;
    }

	/**
	 * constructor from 2 given account params privilege and type as byte numeric values
	 * @param privilege
	 * @param type
	 */
	public AccountTypePrivilege(byte privilege, byte type)
    {
	try
	{
	    setPrivilege(privilege);
	} catch (AccountTypePrivilegeException ex)
	{
	    Logger.getLogger(AccountTypePrivilege.class.getName()).log(Level.SEVERE, null, ex);
	}
	try
	{
	    setType(type);
	} catch (AccountTypePrivilegeException ex)
	{
	    Logger.getLogger(AccountTypePrivilege.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

	/**
	 * constructor from combined account type byte numeric value [same as in db]
	 * @param combiParam
	 */
	public AccountTypePrivilege(byte combiParam)
    {
	try
	{
	    setPrivilege((byte) ((combiParam >> 4) & (byte) 0x0F));
	} catch (AccountTypePrivilegeException ex)
	{
	    Logger.getLogger(AccountTypePrivilege.class.getName()).log(Level.SEVERE, null, ex);
	}
	try
	{
	    setType((byte) (combiParam & 0x0F));
	} catch (AccountTypePrivilegeException ex)
	{
	    Logger.getLogger(AccountTypePrivilege.class.getName()).log(Level.SEVERE, null, ex);
	}
    }

	/**
	 *
	 * @return privilege
	 */
	public byte getPrivilege()
    {
		return privilege;
    }

	/**
	 *
	 * @return privilege string name
	 */
	public String getPrivilegeName()
    {
		String name;
		switch (privilege)
		{
			case PRIVILEGE_BASIC:
				name = AccountPrivilege.BASIC.toString();
				break;
			case PRIVILEGE_PREMIUM:
				name = AccountPrivilege.PREMIUM.toString();
				break;
			case PRIVILEGE_SUPERVISOR:
				name = AccountPrivilege.SUPERVISOR.toString();
				break;
			default:
			name = "Not Set";
			break;
		}
		return name;
    }
	
	/**
	 *
	 * @return privilege enum value
	 */
	public AccountPrivilege getPrivilegeEnum()
    {
		AccountPrivilege privilegeEnum;
		switch (privilege)
		{
			case PRIVILEGE_BASIC:
				privilegeEnum = AccountPrivilege.BASIC;
				break;
			case PRIVILEGE_PREMIUM:
				privilegeEnum = AccountPrivilege.PREMIUM;
				break;
			case PRIVILEGE_SUPERVISOR:
				privilegeEnum = AccountPrivilege.SUPERVISOR;
				break;
			default:
				privilegeEnum = AccountPrivilege.NOT_SET;
				break;				
		}
		return privilegeEnum;
    }	

	/**
	 *
	 * @param privilege
	 * @throws AccountTypePrivilegeException
	 */
	public final void setPrivilege(byte privilege) throws AccountTypePrivilegeException
    {
		if (privilege < PRIVILEGE_LEVEL_MIN || privilege > PRIVILEGE_LEVEL_MAX)
		{
			throw new AccountTypePrivilegeException("Privilege out of range!");
		}
		this.privilege = privilege;
    }

	/**
	 *
	 * @return type byte value
	 */
	public byte getType()
    {
		return type;
    }

	/**
	 *
	 * @return account type string name
	 */
	public String getTypeName()
    {
		String name;
		switch (type)
		{
			case TYPE_PRIVATE:
				name = AccountType.PRIVATE.toString();
				break;
			case TYPE_COMMERCIAL:
				name = AccountType.COMMERCIAL.toString();
				break;
			case TYPE_ADMIN:
				name = AccountType.ADMINISTRATOR.toString();
				break;
			default:
				name = "Not Set";
				break;
		}
		return name;
    }
	
	/**
	 *
	 * @return account type enum value
	 */
	public AccountType getTypeEnum()
    {
		AccountType nameEnum;
		switch (type)
		{
			case TYPE_PRIVATE:
				nameEnum = AccountType.PRIVATE;
				break;
			case TYPE_COMMERCIAL:
				nameEnum = AccountType.COMMERCIAL;
				break;
			case TYPE_ADMIN:
				nameEnum = AccountType.ADMINISTRATOR;
				break;
			default:
				nameEnum = AccountType.NOT_SET;
				break;
		}
		return nameEnum;
    }	

	/**
	 *
	 * @param type
	 * @throws AccountTypePrivilegeException
	 */
	public final void setType(byte type) throws AccountTypePrivilegeException
    {
		if (type < TYPE_MIN || type > TYPE_MAX)
		{
			throw new AccountTypePrivilegeException("Category out of range!");
		}
		this.type = type;
    }

	/**
	 *
	 * @return combined account type bye value as in db (type+privilege)
	 */
	public byte getCombiParam()
    {
		return (byte) ((this.privilege << 4) | this.type);
    }

    @Override
    public int hashCode()
    {
		int hash = 7;
		hash = 29 * hash + this.privilege;
		hash = 29 * hash + this.type;
		return hash;
    }

    @Override
    public boolean equals(Object obj)
    {
		if (obj == null)
		{
			return false;
		}
		if (getClass() != obj.getClass())
		{
			return false;
		}
		final AccountTypePrivilege other = (AccountTypePrivilege) obj;
		if (this.privilege != other.privilege)
		{
			return false;
		}
		if (this.type != other.type)
		{
			return false;
		}
		return true;
    }

	/**
	 *
	 * @return
	 */
	public String toEnumString()
    {
		return "AccountType{" + "Privilege=" + getPrivilegeEnum() + ", Type=" + getTypeEnum() + ", CombiParam=" + getCombiParam() + '}';
    }	
	
	/**
	 *
	 * @return
	 */
	public String toNameString()
    {
		return "AccountType{" + "Privilege=" + getPrivilegeName() + ", Type=" + getTypeName() + ", CombiParam=" + getCombiParam() + '}';
    }

	@Override
    public String toString()
    {
		return "AccountType{" + "Privilege=" + getPrivilege() + ", Type=" + getType() + ", CombiParam=" + getCombiParam() + '}';
    }	
	
    // Class tests

	/**
	 *
	 * @param args
	 */
	    public static void main(String[] args)
    {
		AccountTypePrivilege accType3 = new AccountTypePrivilege(PRIVILEGE_SUPERVISOR, TYPE_ADMIN);
		System.out.println(accType3.toString());
		AccountTypePrivilege accType2 = new AccountTypePrivilege(PRIVILEGE_PREMIUM, TYPE_ADMIN);
		System.out.println(accType2.toString());
		AccountTypePrivilege accType1 = new AccountTypePrivilege(PRIVILEGE_BASIC, TYPE_ADMIN);
		System.out.println(accType1.toString());
		AccountTypePrivilege accType0 = new AccountTypePrivilege();
		System.out.println(accType0.toEnumString());
		AccountTypePrivilege accTypeDB = new AccountTypePrivilege((byte) 51);
		System.out.println(accTypeDB.toNameString());
		
		AccountTypePrivilege accTypePrivate = new AccountTypePrivilege(PRIVILEGE_BASIC, TYPE_PRIVATE);
		System.out.println(accTypePrivate.toString());		

		AccountTypePrivilege accTypeCom = new AccountTypePrivilege(PRIVILEGE_BASIC, TYPE_COMMERCIAL);
		System.out.println(accTypeCom.toString());		
	
	}

}

/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package LAEX.Business.CustomDataType;

import org.neo4j.graphdb.Label;

/**
 * Defines allowed types of nodes/points in graph database
 * @author Lech Jankowski
 */
public enum PointType implements Label
{

	/**
	 * Vendor.business/service node
	 */
	COMMERCIAL,

	/**
	 * point of interest
	 */
	LANDMARK,

	/**
	 * node which leads(is part of) to a different tour
	 */
	PORTAL,

	/**
	 * node with posible various routs 
	 */
	CHOICE,

	/**
	 * node of bend of the trail
	 */
	VERTEX,

	/**
	 * high hierarchy node defining a tour (group of points)
	 */
	TOUR,

	/**
	 * commercial ad node
	 */
	AD,

	/**
	 * default - for error debuging 
	 */
	DEFAULT;
}
/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Business.CustomDataType;


import org.neo4j.graphdb.RelationshipType;

/**
 * Defines type of relationships between the nodes/points
 * @author Lech Jankowski
 */
public enum PointRelationships implements RelationshipType{

	/**
	 * Defines association of a node/point e.g. node:landmark.point1-BELONGS_TO-node:tour.tour1
	 */
	BELONGS_TO,

	/**
	 * Defines outgoing path connection of a node/point e.g. node:landmark.point1-LEADS_UP-node:landmark.point2
	 */
	LEADS_UP,

	/**
	 * Defines incoming path connection of a node/point e.g. node:landmark.point5-LEADS_DOWN-node:landmark.point1
	 */
	LEADS_DOWN,

	/**
	 * Defines bi-derectional path connection of a node/point e.g. node:landmark.point2-LEADS_DOWN-node:landmark.point3
	 */
	LEADS_UP_DOWN,

	/**
	 * Defines connection with commercial/vendor node/point e.g. node.landmark.point5-NERBY_SERVICE-node.commercial.adventure_centre
	 */
	NEARBY_SERVICE,

	/**
	 * Defines connection of commercial node/point or any point with and ad node. e.g. node.landmark.point5-HAS_ADVERT-node.ad.adID='21342134'
	 */
	HAS_ADVERT;
}

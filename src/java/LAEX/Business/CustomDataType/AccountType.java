/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Business.CustomDataType;

/**
 * Defines possible and only valid types of user accounts.
 * See LAEX documentation for user account details
 * @author Lech Jankowski
 */
public enum AccountType
{

	/**
	 * Defines user type for standard user (tour/landmark explorer)
	 */
	PRIVATE,

	/**
	 * Defines user type for standard commercial user = vendor
	 */
	COMMERCIAL,

	/**
	 * Defines system administrators type
	 */
	ADMINISTRATOR,

	/**
	 * default
	 */
	NOT_SET
}
/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package LAEX.Command.Factory;

import LAEX.Business.User.User;
import LAEX.Service.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * not used yet
 * @author Lech Jankowski
 */
public class UserAdd implements Command{

	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
		/*
        String forwardToJsp = "";
        HttpSession session = request.getSession();
        String username = (String) session.getAttribute("newusername");
        String password = (String) session.getAttribute("newuserpass");
        String firstName = request.getParameter("fName");
        String lastName = request.getParameter("sName");
        String phone = request.getParameter("phone");
        String address1 = request.getParameter("address1");
        String address2 = request.getParameter("address2");
        String city = request.getParameter("city");
        String country = request.getParameter("country");
        
        
        UserService us = new UserService();
        int rows = us.UserAdd(username, password, firstName, lastName, phone, address1, address2, city, country);
        User u = us.login(username, password);
        
            
            if (rows != -1 && u != null) {
                session = request.getSession();
                String clientSessionId = session.getId();
                session.setAttribute("loggedSessionId", clientSessionId);
                session.setAttribute("user", u);                
                              
                forwardToJsp = "/JSP/userAccount.jsp";
            } else {
                forwardToJsp = "/JSP/error.jsp";
            }
        return forwardToJsp;
								*/ return null;
    }

}

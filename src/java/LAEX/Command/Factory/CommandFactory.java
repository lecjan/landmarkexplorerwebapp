/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Command.Factory;

/**
 *
 * @author Lech Jankowski
 */
public class CommandFactory {
 
	/**
	 *
	 * @param commandStr
	 * @return command
	 */
	public Command createCommand(String commandStr) 
    {
    	Command command = null;
		switch (commandStr) {
			case "Login"			:	command = new UserLogin();
										break;
			case "Register"			:	command = new UserRegister();
										break;
			case "Logout"			:	command = new UserLogout();
										break;
			case "addUser"			:	command = new UserAdd();
										break;
			case "editUser"			:	command = new UserEdit();
										break;
			case "removePoint"		:	command = new GotoRemove();
										break;
			case "updatePoint"		:	command = new PointUpdate();
										break;
			case "editPoint"		:	command = new PointEdit();
										break;
			case "deletePoint"		:	command = new PointDelete();
										break;			
			case "createPoint"		:	command = new PointAdd();
										break;					
			case "gotoHomePage"		:	command = new GotoHomePage();
										break;
			case "gotoLandingPage"	:	command = new GotoLandingPage();
										break;
			case "notset"			:	command = new GotoHomePage();
										break;
			default					:	command = new GotoLandingPage();
										break;
		}
    	return command;
    }     
}

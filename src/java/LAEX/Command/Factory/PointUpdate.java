/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Command.Factory;

import LAEX.Business.CustomDataType.GeoPoint;
import LAEX.Business.CustomDataType.PointType;
import LAEX.Business.MapPoint;
import LAEX.Service.PointService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Updates point/node and redirects to NodeList.jsp
 * @author Lech Jankowski
 */
public class PointUpdate implements Command{

	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
		
		System.out.println("1.savePoint method start:");
		
        String forwardToJsp;
        HttpSession session = request.getSession();
        String clientSessionId = session.getId();
        String storedSessionId = (String) session.getAttribute("loggedSessionId");
        if(clientSessionId.equals(storedSessionId)) {

// !!!			
			System.out.println("2.savePoint:");
			System.out.println("param1:"+request.getParameter("pointID"));
			System.out.println("param2:"+request.getParameter("name"));
			System.out.println("param3:"+request.getParameter("description"));
			System.out.println("param4:"+request.getParameter("type"));
			System.out.println("param5:"+request.getParameter("latitude"));
			System.out.println("param6:"+request.getParameter("longitude"));
			
			int pointID = Integer.parseInt(request.getParameter("pointID"));
			String name = request.getParameter("name");
			String description = request.getParameter("description");
			PointType type = PointType.valueOf(request.getParameter("type"));
			double latitude = Double.parseDouble(request.getParameter("latitude"));
			double longitude = Double.parseDouble(request.getParameter("longitude"));
			
			System.out.println("3.savePoint new point:before");
			
			MapPoint p = new MapPoint(pointID, name, description, type, new GeoPoint(latitude, longitude));
			
			System.out.println("4.savePoint new point:"+p.toString());
			
			PointService ps = new PointService();
			
			// actuall update
			int rows = ps.updatePoint(p);
			
			System.out.println("savePoint: "+rows+" , "+p.toString());
            if (rows != -1) {
                forwardToJsp = "/NodeList.jsp";
            } else {
                forwardToJsp = "/errorgeneral.jsp";
            }
        } else {
            forwardToJsp = "/errorinvalidsession.jsp";
        }
		
		System.out.println("5.savePoint method: forward"+forwardToJsp);     
		
        return forwardToJsp;

    }
    
}

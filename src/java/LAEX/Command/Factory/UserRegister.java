/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Command.Factory;

import LAEX.Business.CustomDataType.AccountPrivilege;
import LAEX.Business.CustomDataType.AccountType;
import LAEX.Business.User.User;
import LAEX.Service.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Registering new user, signing in and redirecting to adequate page
 * @author Lech Jankowski
 */
public class UserRegister implements Command{

	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@Override
     public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp = "/erroruserregister.jsp";
        
        //The user wants to register in...
        String rUserName = request.getParameter("register-username");
        String rEmail = request.getParameter("register-email");
        String rFirstName = request.getParameter("register-firstname");
        String rLastName = request.getParameter("register-lastname");
        String rPassWord = request.getParameter("register-password");
        String rOptionsAccount = request.getParameter("options-account");
		
        UserService us = new UserService();
		if (us.hasUser(rUserName)) {
			System.out.println("username entered is already used: " + rUserName);
			forwardToJsp = "/errorusernametaken.jsp";
		} else {
			// convert options-account parameter into AccountType and AccountPrivilege
			AccountType accType = AccountType.NOT_SET;
			AccountPrivilege accPrivilege = AccountPrivilege.NOT_SET;
			switch (rOptionsAccount) {
				case "PRIVATE-BASIC":		
											accType = AccountType.PRIVATE;
											accPrivilege = AccountPrivilege.BASIC;
											break;
				case "PRIVATE-PREMIUM":	
											accType = AccountType.PRIVATE;
											accPrivilege = AccountPrivilege.PREMIUM;
											break;
				case "COMMERCIAL-BASIC":
											accType = AccountType.COMMERCIAL;
											accPrivilege = AccountPrivilege.BASIC;
											break;
				case "COMMERCIAL-PREMIUM":   
											accType = AccountType.COMMERCIAL;
											accPrivilege = AccountPrivilege.PREMIUM;
											break;
				default:					
											accType = AccountType.NOT_SET;
											accPrivilege = AccountPrivilege.NOT_SET;
											break;
			} 			
			
			
			// picked username is not used before therefore continue with registration
			User newUser = us.addUser(rUserName, rPassWord, rFirstName, rLastName, "", "", "", "", "", "", "", false, rEmail, true, accType, accPrivilege);
			System.out.println("newUser:"+newUser);
			if (newUser != null) {
				// if register succesful - use same data to login and send to appropriate HomeXXXXX page				
				User userLoggingIn = us.login(rUserName, rPassWord);
				if (userLoggingIn != null) {
					HttpSession session = request.getSession();
					session.setMaxInactiveInterval(60*60);			 // setting timeout to 5mins=5*60 - revise later: 60*60=1h for testing

					String clientSessionId = session.getId();
					session.setAttribute("loggedSessionId", clientSessionId);
					session.setAttribute("user", userLoggingIn);

					AccountType userType = userLoggingIn.getType();
					switch (userType) {
						case ADMINISTRATOR: forwardToJsp = "/HomeAdmin.jsp";
											System.out.println("forward for admin: "+forwardToJsp);
											break;
						case PRIVATE:		forwardToJsp = "/HomePrivate.jsp";					
											System.out.println("forward for private: "+forwardToJsp);
											break;
						case COMMERCIAL:    forwardToJsp = "/HomeCommercial.jsp";
											System.out.println("forward for commercial: "+forwardToJsp);
											break;
						case NOT_SET:       forwardToJsp = "/errorgeneral.jsp";
											System.out.println("forward for not_set: "+forwardToJsp);
											break;
						default:            forwardToJsp = "/errorgeneral.jsp";
											System.out.println("forward for default: "+forwardToJsp);
											break;
					} 

				} else
				{
					forwardToJsp = "/LoginFailure.jsp";	
				}
			} else {
				forwardToJsp = "/erroruserregister.jsp";	
			}
		}
        return forwardToJsp;
    }
}

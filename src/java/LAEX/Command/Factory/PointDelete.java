/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Command.Factory;

import LAEX.Service.PointService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Deletes point/node and redirects to NodeList.jsp
 * @author Lech Jankowski
 */
public class PointDelete implements Command {

	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;
        HttpSession session = request.getSession();
        String clientSessionId = session.getId();
        String storedSessionId = (String) session.getAttribute("loggedSessionId");
        if(clientSessionId.equals(storedSessionId)) {
			int pointID = Integer.parseInt(request.getParameter("pointID"));
			PointService ps = new PointService();
			
			// actuall delete
			int rows = ps.deletePoint(pointID);

			if (rows != -1) {
                forwardToJsp = "/NodeList.jsp";
            } else {
                forwardToJsp = "/errorgeneral.jsp";
            }
        } else {
            forwardToJsp = "/errorinvalidsession.jsp";
        }
        return forwardToJsp;
    }
    
}

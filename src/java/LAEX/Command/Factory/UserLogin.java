/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Command.Factory;



import LAEX.Business.CustomDataType.AccountType;
import LAEX.Business.User.User;
import LAEX.Service.UserService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Login user with his credentials and redirects to adequate page
 * @author Lech Jankowski
 */
public class UserLogin implements Command{

	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        
        String forwardToJsp;
// replace attrivute values if " to "" ' with '' = with null etc        
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        if (username != null && password != null)
        {
            UserService us = new UserService();
            User userLoggingIn = us.login(username, password);
            
            if (userLoggingIn != null)
            {
                HttpSession session = request.getSession();
// timeout				
				session.setMaxInactiveInterval(5*60);			 // setting timeout to 5mins=5*60 - revise later: 60*60=1h for testing
				
                String clientSessionId = session.getId();
                session.setAttribute("loggedSessionId", clientSessionId);
                session.setAttribute("user", userLoggingIn);
                
				AccountType userType = userLoggingIn.getType();
				switch (userType) {
					case ADMINISTRATOR: forwardToJsp = "/HomeAdmin.jsp";
										System.out.println("forward for admin: "+forwardToJsp);
										break;
					case PRIVATE:		forwardToJsp = "/HomePrivate.jsp";					
										System.out.println("forward for private: "+forwardToJsp);
										break;
					case COMMERCIAL:    forwardToJsp = "/HomeCommercial.jsp";
										System.out.println("forward for commercial: "+forwardToJsp);
										break;
					case NOT_SET:       forwardToJsp = "/errorgeneral.jsp";
										System.out.println("forward for not_set: "+forwardToJsp);
										break;
					default:            forwardToJsp = "/errorgeneral.jsp";
										System.out.println("forward for default: "+forwardToJsp);
										break;
				} 
                				
            }
            else
            {
                forwardToJsp = "/LoginFailure.jsp";	
            }
        }
        else 
        {
            forwardToJsp = "/LoginFailure.jsp";	
        }
        
        return forwardToJsp;
    }
}

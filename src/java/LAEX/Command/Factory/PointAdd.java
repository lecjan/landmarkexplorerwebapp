/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Command.Factory;

import LAEX.Business.CustomDataType.GeoPoint;
import LAEX.Business.CustomDataType.PointType;
import LAEX.Business.MapPoint;
import LAEX.Service.PointService;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Ads new Point/Node and redirects to NodeList.jsp
 * @author Lech Jankowski
 */
public class PointAdd implements Command{

	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        String forwardToJsp;
        HttpSession session = request.getSession();
        String clientSessionId = session.getId();
        String storedSessionId = (String) session.getAttribute("loggedSessionId");
        if(clientSessionId.equals(storedSessionId)) {
// !!!			
			System.out.println("1.addPoint:");
			System.out.println("param1:"+request.getParameter("pointID"));
			System.out.println("param2:"+request.getParameter("name"));
			System.out.println("param3:"+request.getParameter("description"));
			System.out.println("param4:"+request.getParameter("type"));
			System.out.println("param5:"+request.getParameter("latitude"));
			System.out.println("param6:"+request.getParameter("longitude"));
			
			int pointID = -1; // must be later set in neo4jdao on max pointID+1 from extra read
			
			String name = request.getParameter("name");
			String description = request.getParameter("description");
			PointType type = PointType.valueOf(request.getParameter("type"));
			double latitude = Double.parseDouble(request.getParameter("latitude"));
			double longitude = Double.parseDouble(request.getParameter("longitude"));
			
			System.out.println("2.addPoint new point:before");
			
			MapPoint p = new MapPoint(pointID, name, description, type, new GeoPoint(latitude, longitude));
			
			System.out.println("3.addPoint new point:"+p.toString());
			
			PointService ps = new PointService();
			
			// actuall adding new point to neo4j graph database
			int rows = ps.createPoint(p);
			
			System.out.println("4. addPoint: "+rows+" , "+p.toString());
			
            if (rows != -1) {
                forwardToJsp = "/NodeList.jsp";
            } else {
                forwardToJsp = "/errorgeneral.jsp";
            }
        } else {
            forwardToJsp = "/errorinvalidsession.jsp";
        }
		
		System.out.println("5.addPoint method: forward"+forwardToJsp);     
		
        return forwardToJsp;

    }
    
}

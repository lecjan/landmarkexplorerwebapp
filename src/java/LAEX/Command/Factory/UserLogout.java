/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Command.Factory;

import LAEX.Messages.AppWarningMessage;
import LAEX.Business.User.User;
import LAEX.Service.UserService;
import static java.lang.System.out;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Signig off the user and redirects to a LAndingPage.jsp (login)
 * @author Lech Jankowski
 */
public class UserLogout implements Command{

	/**
	 *
	 * @param request
	 * @param response
	 * @return
	 */
	@Override
    public String execute(HttpServletRequest request, HttpServletResponse response) {
        
        String forwardToJsp;
        
		HttpSession session = request.getSession();
        String clientSessionId = session.getId();
		User userLoggedIn = (User) session.getAttribute("user");
		System.out.println("Logout:user before :"+userLoggedIn.toString());

        if (userLoggedIn != null)
        {
            UserService us = new UserService();
            User userLoggingOut = us.logout(userLoggedIn.getUserName());
			System.out.println("Logout:user after:"+userLoggingOut.toString());
            
            if (userLoggingOut != null)
            {
				session.invalidate();
				AppWarningMessage.setText("You have logged out succesfully!");				
                forwardToJsp = "/LandingPage.jsp";				
            }
            else
            {
				AppWarningMessage.setText("Errors updating user login status!");				
                forwardToJsp = "/LogoutFailure.jsp";	
            }
        }
        else 
        {
			AppWarningMessage.setText("User object in this session is <null>!");			
            forwardToJsp = "/LogoutFailure.jsp";	
        }
        
        return forwardToJsp;
    }
}
/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Creational.Factory;

import LAEX.Business.CustomDataType.AccountPrivilege;
import LAEX.Business.CustomDataType.AccountType;
import LAEX.Business.User.User;
import LAEX.Business.CustomDataType.GeoPoint;
import LAEX.Business.User.UserAdministratorBasic;
import LAEX.Business.User.UserAdministratorPremium;
import LAEX.Business.User.UserAdministratorSupervisor;
import LAEX.Business.User.UserCommercialBasic;
import LAEX.Business.User.UserCommercialPremium;
import LAEX.Business.User.UserPrivateBasic;
import LAEX.Business.User.UserPrivatePremium;
import LAEX.Business.User.UserTemplate_NOT_SET;

/**
 * Factory class to create user sub class objects (TBC)
 * @author Lech Jankowski
 */
public class UserFactory {

	/**
	 *
	 * @param userID
	 * @param userName
	 * @param accType
	 * @param accPrivilege
	 * @param geoC
	 * @param geoR
	 * @param online
	 * @return
	 */
	public static User createUserAccount(String userID, String userName, AccountType accType, AccountPrivilege accPrivilege, GeoPoint geoC, GeoPoint geoR, boolean online) {
        User uAccount = null;
        switch (accType.toString() + "|" + accPrivilege.toString()) {
        case "PRIVATE|BASIC":
            uAccount = new UserPrivateBasic(userID, userName, geoC, geoR, online);
            break;
 
        case "PRIVATE|PREMIUM":
            uAccount = new UserPrivatePremium(userID, userName, geoC, geoR, online);
            break;
 
        case "COMMERCIAL|BASIC":
            uAccount = new UserCommercialBasic(userID, userName, geoR, online);
            break;

		case "COMMERCIAL|PREMIUM":
            uAccount = new UserCommercialPremium(userID, userName, geoR, online);
            break;
			
        case "ADMINISTRATOR|BASIC":
            uAccount = new UserAdministratorBasic(userID, userName, online);
            break;
 
        case "ADMINISTRATOR|PREMIUM":
            uAccount = new UserAdministratorPremium(userID, userName, online);
            break;

		case "ADMINISTRATOR|SUPERVISOR":
            uAccount = new UserAdministratorSupervisor(userID, userName, online);
            break;
        default:
            uAccount = new UserTemplate_NOT_SET(userID, userName, online);
            break;
        }
        return uAccount;
    }
	
	/**
	 *
	 * @param args
	 */
	public static void main(String[] args) {
        System.out.println(UserFactory.createUserAccount("1","lecjan1", AccountType.PRIVATE, AccountPrivilege.BASIC,null,null,false));System.out.println();
        System.out.println(UserFactory.createUserAccount("2","lecjan2", AccountType.PRIVATE, AccountPrivilege.PREMIUM,null,null,false));System.out.println();
        System.out.println(UserFactory.createUserAccount("3","lecjan3", AccountType.PRIVATE, AccountPrivilege.NOT_SET,null,null,false));System.out.println();
        System.out.println(UserFactory.createUserAccount("4","lecjan4", AccountType.COMMERCIAL, AccountPrivilege.BASIC,null,null,false));System.out.println();
        System.out.println(UserFactory.createUserAccount("5","lecjan5", AccountType.COMMERCIAL, AccountPrivilege.PREMIUM,null,null,false));System.out.println();
        System.out.println(UserFactory.createUserAccount("6","lecjan6", AccountType.COMMERCIAL, AccountPrivilege.BASIC,null,null,false));System.out.println();
        System.out.println(UserFactory.createUserAccount("7","lecjan7", AccountType.ADMINISTRATOR, AccountPrivilege.BASIC,null,null,false));System.out.println();
        System.out.println(UserFactory.createUserAccount("8","lecjan8", AccountType.ADMINISTRATOR, AccountPrivilege.PREMIUM,null,null,false));System.out.println();
        System.out.println(UserFactory.createUserAccount("9","lecjan9", AccountType.ADMINISTRATOR, AccountPrivilege.SUPERVISOR,null,null,false));System.out.println();
        System.out.println(UserFactory.createUserAccount("10","lecjan10", AccountType.ADMINISTRATOR, AccountPrivilege.NOT_SET,null,null,false));System.out.println();
		User usr = UserFactory.createUserAccount("1","lecjan1", AccountType.PRIVATE, AccountPrivilege.BASIC,null,null,false);
		System.out.println(usr);
    }	
}

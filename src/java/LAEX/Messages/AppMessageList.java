/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Messages;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Defines list of AppMessage type objects
 * @author Lech Jankowski
 */
public class AppMessageList {
	
	private ArrayList <AppMessage> appMessages;
	
	/**
	 *
	 */
	public AppMessageList() {
		this.appMessages = null;
	}

	/**
	 *
	 * @param appMessages
	 */
	public AppMessageList(ArrayList<AppMessage> appMessages) {
		this.appMessages = appMessages;
	}

	/**
	 *
	 * @return ArrayList of Messages
	 */
	public ArrayList<AppMessage> getAppMessages() {
		return appMessages;
	}

	/**
	 *
	 * @param appMessages
	 */
	public void setAppMessages(ArrayList<AppMessage> appMessages) {
		this.appMessages = appMessages;
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 17 * hash + Objects.hashCode(this.appMessages);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final AppMessageList other = (AppMessageList) obj;
		if (!Objects.equals(this.appMessages, other.appMessages)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "AppMessageList{" + "appMessages=" + appMessages + '}';
	}
	
	
	

}

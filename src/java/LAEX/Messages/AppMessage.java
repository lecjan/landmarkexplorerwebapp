/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Messages;

import java.util.Objects;

/**
 * Defines Message objects to use within application
 * @author Lech Jankowski
 */
public class AppMessage {
	
	private int id;
	private String message;
	
	/**
	 *
	 */
	public AppMessage() {
		this.id = 0;
		this.message = null;
	}

	/**
	 *
	 * @param id message id
	 * @param message text of a message
	 */
	public AppMessage(int id, String message) {
		this.id = id;
		this.message = message;
	}

	/**
	 *
	 * @return message id
	 */
	public int getId() {
		return id;
	}

	/**
	 *
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 *
	 * @return text message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 *
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 13 * hash + this.id;
		hash = 13 * hash + Objects.hashCode(this.message);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final AppMessage other = (AppMessage) obj;
		if (this.id != other.id) {
			return false;
		}
		if (!Objects.equals(this.message, other.message)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return message;
	}
	
	

}

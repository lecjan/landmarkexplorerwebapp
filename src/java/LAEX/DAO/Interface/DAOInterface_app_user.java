/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.DAO.Interface;

import LAEX.Business.CustomDataType.AccountType;
import LAEX.Exceptions.DAOException;
import LAEX.Business.CustomDataType.GeoPoint;
import LAEX.Business.User.User;
import LAEX.Business.UserGlobalCredentials;
import java.util.ArrayList;

/**
 * Methods accessing app_user table
 * @author Lech Jankowski D00161848
 */

public interface DAOInterface_app_user {

	/**
	 *
	 * @param ugc
	 * @return user
	 * @throws DAOException
	 */
	public User selectByGlobalCredentials(UserGlobalCredentials ugc) throws DAOException;

	/**
	 *
	 * @param userName
	 * @return user
	 * @throws DAOException
	 */
	public User selectByUsername(String userName) throws DAOException;

	/**
	 *
	 * @param usr
	 * @return user
	 * @throws DAOException
	 */
	public User updateLoginON(User usr) throws DAOException;

	/**
	 *
	 * @param usr
	 * @return
	 * @throws DAOException
	 */
	public User updateLoginOFF(User usr) throws DAOException;

	/**
	 *
	 * @param usr
	 * @param geoP
	 * @return true if updated; false otherwise
	 * @throws DAOException
	 */
	public boolean updateGeoCoordinates(User usr, GeoPoint geoP) throws DAOException;	

	/**
	 *
	 * @param usr
	 * @param geoP
	 * @return true if updated; false otherwise
	 * @throws DAOException
	 */
	public boolean updateGeoResidence(User usr, GeoPoint geoP) throws DAOException;

	/**
	 *
	 * @param userName
	 * @return true if username exists; false otherwise
	 * @throws DAOException
	 */
	public boolean hasUsername(String userName) throws DAOException;

	/**
	 *
	 * @return list of all users 
	 * @throws DAOException
	 */
	public ArrayList<User> viewUsersAll() throws DAOException;	

	/**
	 *
	 * @param ofAccType
	 * @return list of users of specific type
	 * @throws DAOException
	 */
	public ArrayList<User> viewUsersOfType(AccountType ofAccType) throws DAOException;

	/**
	 *
	 * @param usr
	 * @return true if application user removed; false otherwise
	 * @throws DAOException
	 */
	public boolean removeApplicationUser(User usr) throws DAOException;
}

/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.DAO.Interface;

import LAEX.Exceptions.DAOException;
import LAEX.Business.UserApplication;

/**
 * Methods accesing user_apps table
 * @author Lech Jankowski D00161848
 */

public interface DAOInterface_user_apps {

	/**
	 *
	 * @param userID unique user id (hash)
	 * @param passWord hashed password
	 * @return UserApplication object
	 * @throws DAOException
	 */
	public UserApplication selectByIDPassword(String userID, String passWord) throws DAOException;

	/**
	 *
	 * @param userApp
	 * @return true if inserted
	 * @throws DAOException
	 */
	public boolean insert(UserApplication userApp) throws DAOException;

	/**
	 *
	 * @param userApp
	 * @param newPassword
	 * @return UserApplication object if updated, null if not
	 * @throws DAOException
	 */
	public UserApplication updatePassword(UserApplication userApp, String newPassword) throws DAOException;

	/**
	 *
	 * @param userApp
	 * @return true if UserAplication object removed
	 * @throws DAOException
	 */
	public boolean remove(UserApplication userApp) throws DAOException;
}

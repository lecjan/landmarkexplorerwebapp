/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.DAO.Interface;

import LAEX.Exceptions.DAOException;
import LAEX.Business.UserGlobalCredentials;

/**
 * Methods accesing global_credentials table
 * @author Lech Jankowski D00161848
 */

public interface DAOInterface_global_credentials {

	/**
	 *
	 * @param userName
	 * @return user global credentials, null if doesn't exists
	 * @throws DAOException
	 */
	public UserGlobalCredentials selectByUsername(String userName) throws DAOException;

	/**
	 *
	 * @param userID
	 * @return user global credentials, null if doesn't exists
	 * @throws DAOException
	 */
	public UserGlobalCredentials selectByUserID(String userID) throws DAOException;

	/**
	 *
	 * @param ugcAccount
	 * @return true when ugc inserted
	 * @throws DAOException
	 */
	public boolean insertUserGlobalCredentialsData(UserGlobalCredentials ugcAccount) throws DAOException;

	/**
	 *
	 * @param ugcAccount
	 * @return true if ugc removed
	 * @throws DAOException
	 */
	public boolean removeUserGlobalCredentialsData(UserGlobalCredentials ugcAccount) throws DAOException;
}

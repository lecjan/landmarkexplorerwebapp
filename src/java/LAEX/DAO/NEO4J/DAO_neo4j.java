/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.DAO.NEO4J;

import LAEX.Business.Ad;
import LAEX.Business.CustomDataType.GeoPoint;
import LAEX.Business.CustomDataType.PointRelationships;
import LAEX.Business.CustomDataType.PointType;
import LAEX.Business.MapPoint;
import LAEX.Business.Tag;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.neo4j.helpers.collection.MapUtil.map;
import org.neo4j.jdbc.Driver;
import org.neo4j.jdbc.Neo4jConnection;

/**
 * Class for connection with neo4j graph database.
 * Contains methods to access and process neo4j database
 * @author Lech Jankowski
 */
public class DAO_neo4j {

	/**
	 * Connect to database
	 * @return Neo4jConnection
	 */
	public Neo4jConnection getConnection(){

		final Driver driver = new Driver();
		final String hostPort = "landmarkexplorer.sb04.stations.graphenedb.com:24789";
		final Properties props = new Properties();
		props.put("user", "landmarkexplorer");
		props.put("password", "9H1mKiRdscjwq1a7bLqK");
		Neo4jConnection conn = null;
		try {
			//Class.forName(driver.toString());
			conn = driver.connect("jdbc:neo4j://" + hostPort, props);
		} catch (SQLException  ex) {
			Logger.getLogger(DAO_neo4j.class.getName()).log(Level.SEVERE, null, ex);
		}
		if (conn != null)
			System.out.println("Connection with Neo4j:landmarkexplorer established!");
		else
			System.out.println("Connection with Neo4j:landmarkexplorer was not possible...!");
		return conn;		
    }

	/**
	 * Release the connection
	 * @param conn
	 */
	public void freeConnection(Neo4jConnection conn){
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
            System.out.println("Failed to free Neo4j connection: " + e.getMessage());
            System.exit(1);
        }
    }
	
	/**
	 * Find maximum pointID value in whole graph database from all point/landmark type nodes
	 * @return maximal used pointID value
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public int getMaxPointID() throws SQLException, ClassNotFoundException{
		int maxPointID = 0;
		
		// Make sure Neo4j Driver is registered
		Class.forName("org.neo4j.jdbc.Driver");

		// Connect
		Neo4jConnection conn = getConnection();

		String query= "MATCH (node) RETURN max(node.pointID) AS max"; // check oll nodes and get maximum value of pointID property
		final PreparedStatement statement = conn.prepareStatement(query);
		final ResultSet result = statement.executeQuery();

		if (result.next()) {
			if (result.getObject("max") != null) {
				maxPointID = (Integer) result.getObject("max");
			}
		}
		return maxPointID;
	}	

	/**
	 * Find maximum adID value in whole graph database from all ad type nodes
	 * @return maximal value of adID used in db
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public int getMaxAdID() throws SQLException, ClassNotFoundException{
		int maxAdID = 0;
		
		// Make sure Neo4j Driver is registered
		Class.forName("org.neo4j.jdbc.Driver");

		// Connect
		Neo4jConnection conn = getConnection();

		String query= "MATCH (node) RETURN max(node.adID) AS max"; // check oll nodes and get maximum value of pointID property
		final PreparedStatement statement = conn.prepareStatement(query);
		final ResultSet result = statement.executeQuery();

		if (result.next()) {
			if (result.getObject("max") != null) {
				maxAdID = (Integer) result.getObject("max");
			} 
		}
		return maxAdID;
	}	
	
	/**
	 * Find maximum tagID value in whole graph database from all tagg type nodes
	 * @return maximal used tagID in graph db
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public int getMaxTagID() throws SQLException, ClassNotFoundException{
		int maxTagID = 0;
		
		// Make sure Neo4j Driver is registered
		Class.forName("org.neo4j.jdbc.Driver");

		// Connect
		Neo4jConnection conn = getConnection();

		String query= "MATCH (node) RETURN max(node.tagID) AS max"; // check oll nodes and get maximum value of pointID property
		final PreparedStatement statement = conn.prepareStatement(query);
		final ResultSet result = statement.executeQuery();

		if (result.next()) {
			if (result.getObject("max") != null) {
				maxTagID = (Integer) result.getObject("max");
			} 
		}
		return maxTagID;
	}		

	/**
	 * Creating and inserting new node/point for landmark/tour of type MapPoint
	 * @param p
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public void createPointNode(MapPoint p) throws SQLException, ClassNotFoundException{
		int rowsAfected = -1;
		// Make sure Neo4j Driver is registered
		Class.forName("org.neo4j.jdbc.Driver");

		// Connect
		Neo4jConnection conn = getConnection();

		// create map
		HashMap<String, Object> pointMap=new HashMap<String, Object>();

		pointMap.put("pointID", p.getPointID());
		pointMap.put("name", p.getName());
		pointMap.put("description", p.getDescription());
		pointMap.put("type",p.getType().toString());
		pointMap.put("geoLat",p.getGeoLocation().getLatitude());
		pointMap.put("geoLon",p.getGeoLocation().getLongitude());

		Map<String, Object> params = map("1", pointMap);

		String query= "CREATE (point:"+p.getType().toString()+" {1}) ";

		final PreparedStatement statement = conn.prepareStatement(query);

		for (Map.Entry<String, Object> entry : params.entrySet()) {
			int index = Integer.parseInt(entry.getKey());
			statement.setObject(index, entry.getValue());
		}

	 final ResultSet result = statement.executeQuery();
	 System.out.println("createPointNode"+result.toString());
	// dbl check what gives a rows number. At the moment create does not return anything
	}	

	/**
	 * Creating and instering new Ad type node
	 * @param aAd
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public void createAdNode(Ad aAd) throws SQLException, ClassNotFoundException{

		// Make sure Neo4j Driver is registered
		Class.forName("org.neo4j.jdbc.Driver");

		// Connect
		Neo4jConnection conn = getConnection();

		// create map
		HashMap<String, Object> adMap=new HashMap<String, Object>();
		
		adMap.put("adID", aAd.getAdID());
		adMap.put("extUrl", aAd.getExtUrl());
		adMap.put("imgUrl", aAd.getImgUrl());
		adMap.put("text",aAd.getText());

		Map<String, Object> params = map("1", adMap);

		String query= "CREATE (point:AD {1})";

		final PreparedStatement statement = conn.prepareStatement(query);

		for (Map.Entry<String, Object> entry : params.entrySet()) {
			int index = Integer.parseInt(entry.getKey());
			statement.setObject(index, entry.getValue());
		}

	 final ResultSet result = statement.executeQuery();
	 System.out.println("createAdNode"+result.toString());
	}	
	
	/*
	public Tag(int tagID, String name)	
	*/

	/**
	 * Creating and inserting new Tag type node
	 * @param aTag
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	
	
	public void createTagNode(Tag aTag) throws SQLException, ClassNotFoundException{

		// Make sure Neo4j Driver is registered
		Class.forName("org.neo4j.jdbc.Driver");

		// Connect
		Neo4jConnection conn = getConnection();

		// create map
		HashMap<String, Object> tagMap=new HashMap<String, Object>();
		
		tagMap.put("tagID", aTag.getTagID());
		tagMap.put("name", aTag.getName());

		Map<String, Object> params = map("1", tagMap);

		String query= "CREATE (point:TAG {1})";

		final PreparedStatement statement = conn.prepareStatement(query);

		for (Map.Entry<String, Object> entry : params.entrySet()) {
			int index = Integer.parseInt(entry.getKey());
			statement.setObject(index, entry.getValue());
		}

	 final ResultSet result = statement.executeQuery();
	 System.out.println("createTagNode"+result.toString());
	}		
	
	/**
	 * Creating new relantionship between the nodes with distance - universal use for all MapPoint nodes
	 * @param p1 MapPoint type node 1
	 * @param p2 MapPoint type node 2
	 * @param distanceKM distance between MapPoint location in KM
	 * @param relationshipType type of relationship (LEADS_UP,LEAD_DOWN,LEAD_UPDOWN)
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void createPathRelationship(MapPoint p1, MapPoint p2, double distanceKM, PointRelationships relationshipType) throws ClassNotFoundException, SQLException {
		// Make sure Neo4j Driver is registered
		Class.forName("org.neo4j.jdbc.Driver");
		// Connect
		Neo4jConnection conn = getConnection();

		String query = "MATCH (a:" + p1.getType() + " {pointID:" + p1.getPointID() + "}),(b:" + p2.getType() + " {pointID:" + p2.getPointID() + "}) CREATE (a) -[r:" + relationshipType + " {distanceKM:" + distanceKM+ "}]-> (b) RETURN a,r,b";


		final PreparedStatement statement = conn.prepareStatement(query);
		final ResultSet result = statement.executeQuery();
	}

	/**
	 * Creating new relantionship between the nodes - universal use for all MapPoint nodes
	 * @param p1 MapPoint type node 1
	 * @param p2 MapPoint type node 2
	 * @param relationshipType type of relationship (BELONGS_TO,NEARBY_SERVICE,CHOICE)
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public void createRelationship(MapPoint p1, MapPoint p2, PointRelationships relationshipType) throws ClassNotFoundException, SQLException {
		// Make sure Neo4j Driver is registered
		Class.forName("org.neo4j.jdbc.Driver");
		// Connect
		Neo4jConnection conn = getConnection();

		String query = "MATCH (a:" + p1.getType() + " {pointID:" + p1.getPointID() + "}),(b:" + p2.getType() + " {pointID:" + p2.getPointID() + "}) CREATE (a) -[r:" + relationshipType + "]-> (b) RETURN a,r,b";

		final PreparedStatement statement = conn.prepareStatement(query);
		final ResultSet result = statement.executeQuery();
	}

	/**
	 * Gets all MapPoint type node as an ArrayList
	 * @return ArrayList of all MapPoint nodes existing
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 */
	public ArrayList<MapPoint> viewPointsAll() throws SQLException, ClassNotFoundException{

		ArrayList<MapPoint> pointList = new ArrayList<MapPoint>();
		MapPoint aPoint = null;

		// Make sure Neo4j Driver is registered
		Class.forName("org.neo4j.jdbc.Driver");

		// Connect
		Neo4jConnection conn = getConnection();

		String query= "MATCH p WHERE p:LANDMARK OR p:COMMERCIAL RETURN p"; // p is just a reference and means nothing and everything about the node

		final PreparedStatement statement = conn.prepareStatement(query);
		final ResultSet result = statement.executeQuery();

		while (result.next()) {
			Map<String, Object> point = (Map<String, Object>) result.getObject("p");

			int pointID = (Integer) point.get("pointID");
			String name = (String) point.get("name");
			String description = (String) point.get("description");	
			PointType pointType;
			pointType = PointType.valueOf( (String) point.get("type") );
			double lat = (Double) point.get("geoLat");
			double lon = (Double) point.get("geoLon");

			aPoint = new MapPoint(pointID, name, description, pointType, new GeoPoint(lat,lon));

			pointList.add(aPoint);
		}
		return pointList;
	}		

	/**
	 * Gets MapPoint of given pointID from  neo4j db
	 * @param pointID
	 * @return MapPoint
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public MapPoint selectPointById(int pointID) throws ClassNotFoundException, SQLException {
	
		// Make sure Neo4j Driver is registered
		Class.forName("org.neo4j.jdbc.Driver");
		// Connect
		Neo4jConnection conn = getConnection();

		// map for query prepared statement
		Map<String, Object> params = map("1", pointID);

		String query = "MATCH (n) WHERE n.pointID ={1} RETURN n";

		final PreparedStatement statement = conn.prepareStatement(query);
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			int index = Integer.parseInt(entry.getKey());
			statement.setObject(index, entry.getValue());
		}
		final ResultSet result = statement.executeQuery();
		
		// setting back new amended point object
		MapPoint aPoint = null;
		while (result.next()) {
			Map<String, Object> bufPoint = (Map<String, Object>) result.getObject("n");

			//int pointID = (Integer) bufPoint.get("pointID");
			String name = (String) bufPoint.get("name");
			String description = (String) bufPoint.get("description");	
			PointType pointType = PointType.valueOf( (String) bufPoint.get("type") );
			double lat = (Double) bufPoint.get("geoLat");
			double lon = (Double) bufPoint.get("geoLon");

			aPoint = new MapPoint(pointID, name, description, pointType, new GeoPoint(lat,lon));		
		}
		return aPoint;
	}		
	
	/**
	 * Sets property for given MapPoint
	 * @param inPoint MapPoint object
	 * @param propertyName name of property to be set
	 * @param propertyValue string or number property value
	 * @return new Updated MapPoint, null if set fails
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public MapPoint setPointProperty(MapPoint inPoint, String propertyName, Object propertyValue) throws ClassNotFoundException, SQLException {
	
		// Make sure Neo4j Driver is registered
		Class.forName("org.neo4j.jdbc.Driver");
		// Connect
		Neo4jConnection conn = getConnection();

		// map for query prepared statement
		Map<String, Object> params = map("1", propertyValue);
		
		String query = "MATCH (modPoint:" + inPoint.getType().toString() + ") WHERE modPoint.pointID = " + inPoint.getPointID() + " SET modPoint." + propertyName + "={1} RETURN modPoint";
		final PreparedStatement statement = conn.prepareStatement(query);
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			int index = Integer.parseInt(entry.getKey());
			statement.setObject(index, entry.getValue());
		}
		final ResultSet result = statement.executeQuery();
		
		// setting back new amended point object
		MapPoint aPoint = null;
		while (result.next()) {
			Map<String, Object> bufPoint = (Map<String, Object>) result.getObject("modPoint");

			int pointID = (Integer) bufPoint.get("pointID");
			String name = (String) bufPoint.get("name");
			String description = (String) bufPoint.get("description");	
			PointType pointType = PointType.valueOf( (String) bufPoint.get("type") );
			double lat = (Double) bufPoint.get("geoLat");
			double lon = (Double) bufPoint.get("geoLon");
			
			aPoint = new MapPoint(pointID, name, description, pointType, new GeoPoint(lat,lon));		
		}
		return aPoint;
	}	

	/**
	 * Removes  a node/point from neo4j db of a given pointID
	 * @param pointID unique point ID
	 * @return no of rows affected, 1 - if deleted, 0 if node not found
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public int deletePointById(int pointID) throws ClassNotFoundException, SQLException {
	
		// Make sure Neo4j Driver is registered 
		Class.forName("org.neo4j.jdbc.Driver");
		// Connect
		Neo4jConnection conn = getConnection();

		// map for query prepared statement
		Map<String, Object> params = map("1", pointID);

		// delete linked relations with a node itself
		// String query = "MATCH (n) OPTIONAL MATCH (n)-[r]-() WHERE n.pointID ={1} DELETE r,n";

		// delete node itself
		String query = "MATCH (n) WHERE n.pointID ={1} DELETE n";
		
		final PreparedStatement statement = conn.prepareStatement(query);
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			int index = Integer.parseInt(entry.getKey());
			statement.setObject(index, entry.getValue());
		}
		final int rows = statement.executeUpdate();
		

		return rows;
	}			

	/**
	 * Tests
	 * @param args
	 */
	public static void main (String [] args) {
		DAO_neo4j dao_neo4j = new DAO_neo4j();
		try {
			/*
			int i = dao_neo4j.getMaxPointID();
			System.out.println("getMaxPointID = "+i);
			
			int maxAdID = dao_neo4j.getMaxAdID();
			System.out.println("getMaxAdID = "+maxAdID);			
			Ad anAdvert = new Ad(maxAdID + 1, "http://www.linkedin.com", "http://localhost:8084/LandmarkExplorerWebApp/adverts/advert2.png", "sample advertisement nr 2");
			dao_neo4j.createAdNode(anAdvert);

			Tag aTag = null;
			int maxTagID;
			
			maxTagID = dao_neo4j.getMaxTagID();
			System.out.println("getMaxTagID = "+maxTagID);			
			aTag = new Tag(maxTagID + 1, "RUINS");
			dao_neo4j.createTagNode(aTag);			
			
			maxTagID = dao_neo4j.getMaxTagID();
			System.out.println("getMaxTagID = "+maxTagID);			
			aTag = new Tag(maxTagID + 1, "SEASIDE WALKS");
			dao_neo4j.createTagNode(aTag);			

			maxTagID = dao_neo4j.getMaxTagID();
			System.out.println("getMaxTagID = "+maxTagID);			
			aTag = new Tag(maxTagID + 1, "BIRD WATCHING");
			dao_neo4j.createTagNode(aTag);			

			maxTagID = dao_neo4j.getMaxTagID();
			System.out.println("getMaxTagID = "+maxTagID);			
			aTag = new Tag(maxTagID + 1, "HIKING");
			dao_neo4j.createTagNode(aTag);			
			*/
			
			
			MapPoint p101 = new MapPoint(1001, "Commercial Place 1001", "B&B 1001", PointType.COMMERCIAL, new GeoPoint(123,145));
			dao_neo4j.createPointNode(p101);
			MapPoint p201 = new MapPoint(302, "Landmark 302", "some desc 302", PointType.LANDMARK, new GeoPoint(124,155));
			dao_neo4j.createPointNode(p201);
			MapPoint p301 = new MapPoint(303, "Landmark 303", "some desc 303", PointType.LANDMARK, new GeoPoint(167,159));
			dao_neo4j.createPointNode(p301);
			MapPoint p401 = new MapPoint(304, "Landmark 304", "some desc 304", PointType.LANDMARK, new GeoPoint(167,159));
			dao_neo4j.createPointNode(p401);

			MapPoint p501 = new MapPoint(2001, "Tour 2001", "Tour with 3 landmarks and B&B 2001", PointType.TOUR, new GeoPoint(123,145));
			dao_neo4j.createPointNode(p501);	
			
			dao_neo4j.createPathRelationship(p101,p201,.5, PointRelationships.NEARBY_SERVICE);
			dao_neo4j.createPathRelationship(p201,p301,2, PointRelationships.LEADS_UP);
			dao_neo4j.createPathRelationship(p301,p401,3, PointRelationships.LEADS_UP);
			dao_neo4j.createPathRelationship(p401,p201,4, PointRelationships.LEADS_UP_DOWN);

			dao_neo4j.createRelationship(p201,p501, PointRelationships.BELONGS_TO);
			dao_neo4j.createRelationship(p301,p501, PointRelationships.BELONGS_TO);
			dao_neo4j.createRelationship(p401,p501, PointRelationships.BELONGS_TO);

			/*
			Point p5 = setPointProperty(p4, "description", "Tour over the Carligford Bay");
			
			System.out.println(" after2setProperty: "+p12.toString());			
			*/
			
		} catch (SQLException ex) {
			Logger.getLogger(DAO_neo4j.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(DAO_neo4j.class.getName()).log(Level.SEVERE, null, ex);
		}
    }		
	
}

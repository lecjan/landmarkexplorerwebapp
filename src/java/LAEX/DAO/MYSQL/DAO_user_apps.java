/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.DAO.MYSQL;

import LAEX.Exceptions.DAOException;
import LAEX.DAO.Interface.DAOInterface_user_apps;
import LAEX.Business.UserApplication;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 *
 * @author Lech Jankowski D00161848
 */

public class DAO_user_apps extends DAO_mysql implements DAOInterface_user_apps {   

	/**
	 *
	 * @param userID
	 * @param passWord
	 * @return
	 * @throws DAOException
	 */
	@Override
        public UserApplication selectByIDPassword(String userID, String passWord) throws DAOException
    {
		UserApplication ua = null; // User is an abstract, later create appropriate UserType
		
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            con = getConnection();
            if (con==null) return null; // when there is no connection user is null

			String query = "SELECT * FROM user_apps WHERE userID = ? AND password = SHA2(?,256)";
            
			ps = con.prepareStatement(query);
            ps.setString(1, userID);
			String pwdPlainSaltedString = userID + passWord;
			ps.setString(2, pwdPlainSaltedString);  

			rs = ps.executeQuery();

			if(rs.next()){     
                // userID already given
				String appSymbol = rs.getString("app_id");
				ua = new UserApplication(userID, passWord, appSymbol);
            } 
        } catch (SQLException e) {
                throw new DAOException("DAOUserGlobalCredentials.selectUserGlobalCredentials().try: " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                    throw new DAOException("DAOUserGlobalCredentials.selectUserGlobalCredentials().finally: " + e.getMessage());
            }
        }
        return ua; 
    }
		
	/**
	 *
	 * @param userApp
	 * @return
	 * @throws DAOException
	 */
	@Override
    public boolean insert(UserApplication userApp) throws DAOException {
        int rowsInserted = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = null;
			try {
				con = getConnection();
				if (con==null) return false;				
				query = "INSERT INTO user_apps (userID, password, app_id) VALUES(?,?,?)";

				ps = con.prepareStatement(query);
				ps.setString(1, userApp.getUserID());
				ps.setString(2, userApp.getPassword());
				ps.setString(3, "LAEX"); // hardcoded symbol for application id as coming from this web application
				
				rowsInserted = ps.executeUpdate();

			} catch (SQLException e) {
				throw new DAOException("DAOUserApplication:insert():): try" + e.getMessage());
			} finally {
				try {
					if (rs != null) {
						rs.close();
					}
					if (ps != null) {
						ps.close();
					}
					if (con != null) {
						freeConnection(con);
					}
				} catch (SQLException e) {
					throw new DAOException("DAOUserApplication:insert():: finally" + e.getMessage());
				}
			}
        return (rowsInserted > 0); 
	};
	
	/**
	 *
	 * @param userApp
	 * @param newPassword
	 * @return
	 * @throws DAOException
	 */
	@Override
    public UserApplication updatePassword(UserApplication userApp, String newPassword) throws DAOException {
        int rowsInserted = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = null;
			try {
				con = getConnection();
				if (con==null) return null;				
				query = "UPDATE user_apps SET password = SHA2(?,256) WHERE userID = ? AND appID = ?";

				ps = con.prepareStatement(query);
				ps.setString(1, newPassword);
				ps.setString(2, userApp.getUserID());
				ps.setString(3, "LAEX"); // hardcoded symbol for application id as coming from this web application
				
				rowsInserted = ps.executeUpdate();

			} catch (SQLException e) {
				throw new DAOException("DAOUserApplication:updatePassword():): try" + e.getMessage());
			} finally {
				try {
					if (rs != null) {
						rs.close();
					}
					if (ps != null) {
						ps.close();
					}
					if (con != null) {
						freeConnection(con);
					}
				} catch (SQLException e) {
					throw new DAOException("DAOUserApplication:updatePassword():: finally" + e.getMessage());
				}
			}
        return userApp; 
	};
	
	/**
	 *
	 * @param userApp
	 * @return
	 * @throws DAOException
	 */
	@Override
    public boolean remove(UserApplication userApp) throws DAOException {
		int rowsDeleted = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;		
		try {
				con = getConnection();
				if (con==null) return false;					
                String query = "DELETE FROM user_apps WHERE userID = ? and appID = ?";
                ps = con.prepareStatement(query);
				ps.setString(1, userApp.getUserID());
				ps.setString(2, "LAEX"); // hardcoded symbol for application id as coming from this web application
                rowsDeleted = ps.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException("UserGlobalCredentialsDAO:removeUserGlobalCredentialsData(): try" + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DAOException("UserGlobalCredentialsDAO:removeUserGlobalCredentialsData(): finally" + e.getMessage());
            }
        }        
        return (rowsDeleted > 0);
	};		

}

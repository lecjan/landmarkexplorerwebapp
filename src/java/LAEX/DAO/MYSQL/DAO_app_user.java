/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.DAO.MYSQL;

import LAEX.Creational.Factory.UserFactory;
import LAEX.Exceptions.DAOException;
import LAEX.DAO.Interface.DAOInterface_app_user;
import LAEX.Business.User.User;
import LAEX.Business.CustomDataType.AccountPrivilege;
import LAEX.Business.CustomDataType.AccountType;
import LAEX.Business.CustomDataType.AccountTypePrivilege;
import LAEX.Business.CustomDataType.GeoPoint;
import LAEX.Business.UserGlobalCredentials;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;




/**
 *
 * @author Lech Jankowski D00161848
 */

public class DAO_app_user extends DAO_mysql implements DAOInterface_app_user {   

	/**
	 *
	 * @param ugc
	 * @return
	 * @throws DAOException
	 */
	@Override
    public User selectByGlobalCredentials(UserGlobalCredentials ugc) throws DAOException 
    {
		User usr = null; // User is an abstract, later create appropriate UserType
		
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            con = getConnection();
            if (con==null) return null; // when there is no connection user is null
            
            String query = "SELECT * FROM app_user WHERE userID = ?";

            ps = con.prepareStatement(query);
            ps.setString(1, ugc.getUserID());
			rs = ps.executeQuery();
            if(rs.next()){     
				byte acc_type = rs.getByte("acc_type");		
				double geo_coordinates_lat = rs.getDouble("geo_coordinates_lat"); 
				double geo_coordinates_lon = rs.getDouble("geo_coordinates_lon"); 
				double geo_residence_lat = rs.getDouble("geo_residence_lat"); 
				double geo_residence_lon = rs.getDouble("geo_residence_lon"); 
				boolean online = rs.getBoolean("online");
				
				// convert to GeoPoint
				System.out.println("coord:" + geo_coordinates_lat + geo_coordinates_lon);
				System.out.println("resid:" + geo_residence_lat + geo_residence_lon);
				GeoPoint geoC = new GeoPoint(geo_coordinates_lat,geo_coordinates_lon);				
				GeoPoint geoR = new GeoPoint(geo_residence_lat,geo_residence_lon);				
				
				AccountType accType = new AccountTypePrivilege(acc_type).getTypeEnum();
				AccountPrivilege accPrivilege = new AccountTypePrivilege(acc_type).getPrivilegeEnum();
				usr = UserFactory.createUserAccount(rs.getString("userID"), rs.getString("username"), accType, accPrivilege, geoC, geoR, online);
				
				// retrieve specific User Type data TBC
				switch (usr.getType()) {
					case ADMINISTRATOR: 
										break;
					case PRIVATE:							
										break;
					case COMMERCIAL: 
										break;
					case NOT_SET:
										break;
				} 
			}
        } catch (SQLException e) {
                throw new DAOException("DAOUserGlobalCredentials.selectUserGlobalCredentials().try: " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                    throw new DAOException("DAOUserGlobalCredentials.selectUserGlobalCredentials().finally: " + e.getMessage());
            }
        }
        return usr; 
    }
	
	/**
	 *
	 * @return
	 * @throws DAOException
	 */
	@Override
    public ArrayList<User> viewUsersAll() throws DAOException 
    {
		User usr = null; // User is an abstract, later create appropriate UserType
		ArrayList<User> userList = new ArrayList<User>();
		
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            con = getConnection();
            if (con==null) return null; // when there is no connection user is null
            
            String query = "SELECT * FROM app_user";

            ps = con.prepareStatement(query);
			rs = ps.executeQuery();

			while (rs.next()){     
				byte acc_type = rs.getByte("acc_type");		
				double geo_coordinates_lat = rs.getDouble("geo_coordinates_lat"); 
				double geo_coordinates_lon = rs.getDouble("geo_coordinates_lon"); 
				double geo_residence_lat = rs.getDouble("geo_residence_lat"); 
				double geo_residence_lon = rs.getDouble("geo_residence_lon"); 
				boolean online = rs.getBoolean("online");
				
				// convert to GeoPoint
				System.out.println("coord:" + geo_coordinates_lat + geo_coordinates_lon);
				System.out.println("resid:" + geo_residence_lat + geo_residence_lon);
				GeoPoint geoC = new GeoPoint(geo_coordinates_lat,geo_coordinates_lon);				
				GeoPoint geoR = new GeoPoint(geo_residence_lat,geo_residence_lon);				
				
				AccountType accType = new AccountTypePrivilege(acc_type).getTypeEnum();
				AccountPrivilege accPrivilege = new AccountTypePrivilege(acc_type).getPrivilegeEnum();
				usr = UserFactory.createUserAccount(rs.getString("userID"), rs.getString("username"), accType, accPrivilege, geoC, geoR, online);
				// retrieve specific User Type data TBC
				switch (usr.getType()) {
					case ADMINISTRATOR: 
										break;
					case PRIVATE:							
										break;
					case COMMERCIAL: 
										break;
					case NOT_SET:
										break;
				} 
				userList.add(usr);				

			}
        } catch (SQLException e) {
                throw new DAOException("DAOUserGlobalCredentials.selectUserGlobalCredentials().try: " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                    throw new DAOException("DAOUserGlobalCredentials.selectUserGlobalCredentials().finally: " + e.getMessage());
            }
        }
        return userList; 
    }	
	
	/**
	 *
	 * @param ofAccType
	 * @return
	 * @throws DAOException
	 */
	@Override
    public ArrayList<User> viewUsersOfType(AccountType ofAccType) throws DAOException 
    {
		User usr = null; // User is an abstract, later create appropriate UserType
		ArrayList<User> userList = new ArrayList<User>();
		
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            con = getConnection();
            if (con==null) return null; // when there is no connection user is null
            
            String query = "SELECT * FROM app_user";

            ps = con.prepareStatement(query);
			rs = ps.executeQuery();

			while (rs.next()){     
				byte acc_type = rs.getByte("acc_type");		
				double geo_coordinates_lat = rs.getDouble("geo_coordinates_lat"); 
				double geo_coordinates_lon = rs.getDouble("geo_coordinates_lon"); 
				double geo_residence_lat = rs.getDouble("geo_residence_lat"); 
				double geo_residence_lon = rs.getDouble("geo_residence_lon"); 
				boolean online = rs.getBoolean("online");
				
				// convert to GeoPoint
				System.out.println("coord:" + geo_coordinates_lat + geo_coordinates_lon);
				System.out.println("resid:" + geo_residence_lat + geo_residence_lon);
				GeoPoint geoC = new GeoPoint(geo_coordinates_lat,geo_coordinates_lon);				
				GeoPoint geoR = new GeoPoint(geo_residence_lat,geo_residence_lon);				
// look into this ???				
				AccountType accType = new AccountTypePrivilege(acc_type).getTypeEnum();
				AccountPrivilege accPrivilege = new AccountTypePrivilege(acc_type).getPrivilegeEnum();
				if (accType == ofAccType )
				{
				usr = UserFactory.createUserAccount(rs.getString("userID"), rs.getString("username"), accType, accPrivilege, geoC, geoR, online);
				// retrieve specific User Type data TBC
					switch (usr.getType()) {
						case ADMINISTRATOR: 
											break;
						case PRIVATE:							
											break;
						case COMMERCIAL: 
											break;
						case NOT_SET:
											break;
					} 
				userList.add(usr);				
				}
			}
        } catch (SQLException e) {
                throw new DAOException("DAOUserGlobalCredentials.selectUserGlobalCredentials().try: " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                    throw new DAOException("DAOUserGlobalCredentials.selectUserGlobalCredentials().finally: " + e.getMessage());
            }
        }
        return userList; 
    }	
	
	/**
	 *
	 * @param usr
	 * @return
	 * @throws DAOException
	 */
	@Override
    public User updateLoginON(User usr) throws DAOException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;   
        
        if (usr.getUserID() != null) // checking if that is not null as default
        {
            // setting the flag in logins table
            try {
                con = getConnection();
                if (con==null) return usr; // when there is no connection
                String query = "UPDATE app_user SET online = ? WHERE userID = ?";
				/* ORGINAL QUERY - CHANGE BACK TO IT AFTER TESTING
                String query = "UPDATE app_user SET online = ? WHERE userID = ? AND online IS NOT TRUE ";				
				*/		
                ps = con.prepareStatement(query);
				
				//true for logged in
                ps.setBoolean(1, true);  
                ps.setString(2, "" + usr.getUserID() + "");
				
				int rowsUpdated = ps.executeUpdate();   

				if (rowsUpdated == 1) {
                    usr.setLoggedIn(true);
                } else return null; // if online column is true user cannot login twice

			} catch (SQLException e) {
                    throw new DAOException("DAOapp_user.setLoginStatus().try: " + e.getMessage());
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                    if (con != null) {
                        freeConnection(con);
                    }
                } catch (SQLException e) {
                        throw new DAOException("DAOapp_user.setLoginStatus().finally: " + e.getMessage());
                }
            }
        }
        return usr;  
    }    
	
	/**
	 *
	 * @param usr
	 * @return
	 * @throws DAOException
	 */
	@Override
    public User updateLoginOFF(User usr) throws DAOException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;   
		
        if (usr.getUserID() != null) // checking if that is not null as default
        {
            // setting the flag in logins table
            try {
                con = getConnection();
                if (con==null) return usr; // when there is no connection
                String query = "UPDATE app_user SET online = ? WHERE userID = ?";
                ps = con.prepareStatement(query);
				
				// false value for logged off
                ps.setBoolean(1, false);  
                ps.setString(2, "" + usr.getUserID() + "");
				
				int rowsUpdated = ps.executeUpdate();   

				if (rowsUpdated == 1) {
                    usr.setLoggedIn(false);
                } else return null; // if online column is true user cannot login twice

			} catch (SQLException e) {
                    throw new DAOException("DAOapp_user.setLoginStatus().try: " + e.getMessage());
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                    if (con != null) {
                        freeConnection(con);
                    }
                } catch (SQLException e) {
                        throw new DAOException("DAOapp_user.setLoginStatus().finally: " + e.getMessage());
                }
            }
        }
        return usr;  
    }   	
	
	/**
	 *
	 * @param userName
	 * @return
	 * @throws DAOException
	 */
	@Override
    public boolean hasUsername(String userName) throws DAOException 
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            con = getConnection();
            if (con==null) return false; // when there is no connection user is null
            
            String query = "SELECT * FROM app_user WHERE username = ?";

            ps = con.prepareStatement(query);
            ps.setString(1, userName);
			rs = ps.executeQuery();
            if(rs.next()){     
				return true;
			}
        } catch (SQLException e) {
                throw new DAOException("DAOUserGlobalCredentials.hasUsername().try: " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                    throw new DAOException("DAOUserGlobalCredentials.hasUsername().finally: " + e.getMessage());
            }
        }
		
        return false; 
    }	
	
	/**
	 *
	 * @param userName
	 * @return
	 * @throws DAOException
	 */
	@Override
    public User selectByUsername(String userName) throws DAOException 
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
		
		User usr = null;
        
        try {
            con = getConnection();
            if (con==null) return null; // when there is no connection user is null
            
            String query = "SELECT * FROM app_user WHERE username = ?";

            ps = con.prepareStatement(query);
            ps.setString(1, userName);
			rs = ps.executeQuery();
            if(rs.next()){     
				byte acc_type = rs.getByte("acc_type");		
				double geo_coordinates_lat = rs.getDouble("geo_coordinates_lat"); 
				double geo_coordinates_lon = rs.getDouble("geo_coordinates_lon"); 
				double geo_residence_lat = rs.getDouble("geo_residence_lat"); 
				double geo_residence_lon = rs.getDouble("geo_residence_lon"); 
				boolean online = rs.getBoolean("online");
				
				// convert to GeoPoint
				System.out.println("coord:" + geo_coordinates_lat + geo_coordinates_lon);
				System.out.println("resid:" + geo_residence_lat + geo_residence_lon);
				GeoPoint geoC = new GeoPoint(geo_coordinates_lat,geo_coordinates_lon);				
				GeoPoint geoR = new GeoPoint(geo_residence_lat,geo_residence_lon);				
				
				AccountType accType = new AccountTypePrivilege(acc_type).getTypeEnum();
				AccountPrivilege accPrivilege = new AccountTypePrivilege(acc_type).getPrivilegeEnum();
				usr = UserFactory.createUserAccount(rs.getString("userID"), rs.getString("username"), accType, accPrivilege, geoC, geoR, online);
			}
        } catch (SQLException e) {
                throw new DAOException("DAOUserGlobalCredentials.selectByUsername().try: " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                    throw new DAOException("DAOUserGlobalCredentials.selectByUsername().finally: " + e.getMessage());
            }
        }
		
        return usr; 
    }		
	
	/**
	 *
	 * @param usr
	 * @param geoP
	 * @return
	 * @throws DAOException
	 */
	@Override
	public boolean updateGeoCoordinates(User usr, GeoPoint geoP) throws DAOException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;   
        
        if (usr.getUserID() != null) // checking if that is not null as default
        {
            // setting the flag in logins table
            try {
                con = getConnection();
                if (con==null) return false; // when there is no connection
                String query = "UPDATE app_user SET geo_coordinates_lat = ?, geo_coordinates_lon = ?  WHERE userID = ?";
                ps = con.prepareStatement(query);
				
				//true for logged in
                ps.setDouble(1, geoP.getLatitude());  
                ps.setDouble(2, geoP.getLongitude());  
				
				int rowsUpdated = ps.executeUpdate();   

				if (rowsUpdated == 1) {
                } else return true; // if online column is true user cannot login twice

			} catch (SQLException e) {
                    throw new DAOException("DAOapp_user.updateGeoCoordinate().try: " + e.getMessage());
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                    if (con != null) {
                        freeConnection(con);
                    }
                } catch (SQLException e) {
                        throw new DAOException("DAOapp_user.updateGeoCoordinate().finally: " + e.getMessage());
                }
            }
        }
        return false;  
    }    		
	
	/**
	 *
	 * @param usr
	 * @return
	 * @throws DAOException
	 */
	public boolean insert(User usr) throws DAOException
    {
		int rowsInserted = -1;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;   
        
        if (usr.getUserID() != null) // checking if that is not null as default
        {
            // setting the flag in logins table
            try {
                con = getConnection();
                if (con==null) return false; // when there is no connection
				
                String query = "INSERT INTO app_user (userID, username, acc_type, geo_coordinates_lat, "
						+ "geo_coordinates_lon, geo_residence_lat, geo_residence_lon, online) VALUES (?,?,?,?,?,?,?,?)";
				
                ps = con.prepareStatement(query);
				ps.setString(1,usr.getUserID());
				ps.setString(2,usr.getUserName());
				
				// account type and privilege combi column
				AccountTypePrivilege accType = new AccountTypePrivilege(usr.getPrivilegeByte(), usr.getTypeByte());
				ps.setInt(3,accType.getCombiParam());
				
				//ps.setInt(3,51);

				ps.setDouble(4,0);
				ps.setDouble(5,0);
				ps.setDouble(6,0);
				ps.setDouble(7,0);
				ps.setBoolean(8,false);
				
				rowsInserted = ps.executeUpdate();   

			} catch (SQLException e) {
                    throw new DAOException("DAOapp_user.insert().try: " + e.getMessage());
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                    if (con != null) {
                        freeConnection(con);
                    }
                } catch (SQLException e) {
                        throw new DAOException("DAOapp_user.insert().finally: " + e.getMessage());
                }
            }
        }
        return (rowsInserted > 0);  
    }    	
	
	/**
	 *
	 * @param usr
	 * @param geoP
	 * @return
	 * @throws DAOException
	 */
	@Override
	public boolean updateGeoResidence(User usr, GeoPoint geoP) throws DAOException
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;   
        
        if (usr.getUserID() != null) // checking if that is not null as default
        {
            // setting the flag in logins table
            try {
                con = getConnection();
                if (con==null) return false; // when there is no connection
                String query = "UPDATE app_user SET geo_residence_lat = ?, geo_residence_lon = ?  WHERE userID = ?";
                ps = con.prepareStatement(query);
				
				//true for logged in
                ps.setDouble(1, geoP.getLatitude());  
                ps.setDouble(2, geoP.getLongitude());  
				
				int rowsUpdated = ps.executeUpdate();   

				if (rowsUpdated == 1) {
                } else return true; // if online column is true user cannot login twice

			} catch (SQLException e) {
                    throw new DAOException("DAOapp_user.updateGeoResidence().try: " + e.getMessage());
            } finally {
                try {
                    if (rs != null) {
                        rs.close();
                    }
                    if (ps != null) {
                        ps.close();
                    }
                    if (con != null) {
                        freeConnection(con);
                    }
                } catch (SQLException e) {
                        throw new DAOException("DAOapp_user.updateGeoResidence().finally: " + e.getMessage());
                }
            }
        }
        return false;  
    }
	
	/**
	 *
	 * @param usr
	 * @return
	 * @throws DAOException
	 */
	@Override
	public boolean removeApplicationUser(User usr) throws DAOException {
		int rowsDeleted = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;		
		try {
			if (usr == null) return false;
			{ 
				con = getConnection();
                String query = "DELETE FROM app_user WHERE userID = ?";
                ps = con.prepareStatement(query);
                ps.setString(1, usr.getUserID());
                rowsDeleted = ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException("DAOapp_user:removeApplicationUser(): try" + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DAOException("DAOapp_user:removeApplicationUser(): finally" + e.getMessage());
            }
        }        
        return (rowsDeleted > 0);		
	}
	
}

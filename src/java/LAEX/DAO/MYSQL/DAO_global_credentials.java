/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.DAO.MYSQL;

import LAEX.Exceptions.DAOException;
import LAEX.DAO.Interface.DAOInterface_global_credentials;
import LAEX.Business.UserGlobalCredentials;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 *
 * @author Lech Jankowski D00161848
 */

public class DAO_global_credentials extends DAO_mysql implements DAOInterface_global_credentials {   

	/**
	 *
	 * @param userName
	 * @return
	 * @throws DAOException
	 */
	@Override
    public UserGlobalCredentials selectByUsername(String userName) throws DAOException 
    {
        UserGlobalCredentials ugcAccount = new UserGlobalCredentials();
    
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        // try to get salt (which is userID) from the table - assuming userName is unique we only must get 1 row only
        try {
            con = getConnection();
            if (con==null) return ugcAccount; // when there is no connection
            
            String query = "SELECT * FROM global_credentials WHERE username = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, userName);
            rs = ps.executeQuery();

			if(rs.next()){     
                ugcAccount.setUserID(rs.getString("userID"));
                ugcAccount.setUserName(userName);
                ugcAccount.setFirstName(rs.getString("firstName"));
                ugcAccount.setLastName(rs.getString("lastName"));
				ugcAccount.setAddrLine1(rs.getString("addr_line1"));
				ugcAccount.setAddrLine2(rs.getString("addr_line2"));
				ugcAccount.setAddrLine3(rs.getString("addr_line3"));
				ugcAccount.setAddrCity(rs.getString("city"));
				ugcAccount.setAddrRegion(rs.getString("region"));
				ugcAccount.setAddrCountry(rs.getString("country"));
				ugcAccount.setContactPhone(rs.getString("phone"));
				ugcAccount.setContactPhoneConcent(rs.getBoolean("ph_consent"));
				ugcAccount.setContactEmail(rs.getString("email"));
				ugcAccount.setContactEmailConcent(rs.getBoolean("em_consent"));	
            }
        } catch (SQLException e) {
                throw new DAOException("DAOUserGlobalCredentials.selectUserGlobalCredentials().try: " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                    throw new DAOException("DAOUserGlobalCredentials.selectUserGlobalCredentials().finally: " + e.getMessage());
            }
        }
        return ugcAccount; 
    }
	
	/**
	 *
	 * @param userID
	 * @return
	 * @throws DAOException
	 */
	@Override
    public UserGlobalCredentials selectByUserID(String userID) throws DAOException 
    {
        UserGlobalCredentials ugcAccount = new UserGlobalCredentials();
    
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        
        try {
            con = getConnection();
            if (con==null) return null; // when there is no connection
            
            String query = "SELECT * FROM global_credentials WHERE userID = ?";
            //String query = "SELECT * FROM global_credentials WHERE userID = ? AND password = SHA2(?,256)";
            ps = con.prepareStatement(query);
            ps.setString(1, userID);
			//String pwdPlainSaltedString = userID + passWord;
			//ps.setString(2, pwdPlainSaltedString);            
			rs = ps.executeQuery();
            if(rs.next()){     
                ugcAccount.setUserID(userID);
                ugcAccount.setUserName(rs.getString("username"));
                ugcAccount.setFirstName(rs.getString("firstName"));
                ugcAccount.setLastName(rs.getString("lastName"));				
				ugcAccount.setAddrLine1(rs.getString("addr_line1"));
				ugcAccount.setAddrLine2(rs.getString("addr_line2"));
				ugcAccount.setAddrLine3(rs.getString("addr_line3"));
				ugcAccount.setAddrCity(rs.getString("city"));
				ugcAccount.setAddrRegion(rs.getString("region"));
				ugcAccount.setAddrCountry(rs.getString("country"));
				ugcAccount.setContactPhone(rs.getString("phone"));
				ugcAccount.setContactPhoneConcent(rs.getBoolean("ph_consent"));
				ugcAccount.setContactEmail(rs.getString("email"));
				ugcAccount.setContactEmailConcent(rs.getBoolean("em_consent"));
            } else return null;
        } catch (SQLException e) {
                throw new DAOException("DAOUserGlobalCredentials.selectUserGlobalCredentials().try: " + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                    throw new DAOException("DAOUserGlobalCredentials.selectUserGlobalCredentials().finally: " + e.getMessage());
            }
        }
        return ugcAccount;
    }		

	/**
	 *
	 * @param ugcAccount
	 * @return
	 * @throws DAOException
	 */
	@Override
    public boolean insertUserGlobalCredentialsData(UserGlobalCredentials ugcAccount) throws DAOException {
        int rowsInserted = 0;

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = null;

        try {
            con = getConnection();

            // check if userName does not already exist
            query = "SELECT userName FROM global_credentials WHERE userName = ?";
            ps = con.prepareStatement(query);
            ps.setString(1, ugcAccount.getUserName());

            rs = ps.executeQuery();
            if (rs.next()) {
                throw new DAOException("UserGlobalCredentialsDAO:insertUserGlobalCredentialsData():userName " + ugcAccount.getUserName() + " already exists");
            }

			query = "INSERT INTO global_credentials (userID, username, firstname, lastname, addr_line1, addr_line2, addr_line3, city, region, country, phone, ph_consent, email, em_consent) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

			ps = con.prepareStatement(query);
            ps.setString(1, ugcAccount.getUserID());              
            ps.setString(2, ugcAccount.getUserName());       
            ps.setString(3, ugcAccount.getFirstName());  
            ps.setString(4, ugcAccount.getLastName());  			
            ps.setString(5, ugcAccount.getAddrLine1());              
            ps.setString(6, ugcAccount.getAddrLine2());              
            ps.setString(7, ugcAccount.getAddrLine3());              
            ps.setString(8, ugcAccount.getAddrCity());              
            ps.setString(9, ugcAccount.getAddrRegion());
            ps.setString(10, ugcAccount.getAddrCountry());
            ps.setString(11,ugcAccount.getContactPhone());
            ps.setBoolean(12,ugcAccount.isContactPhoneConcent());
            ps.setString(13,ugcAccount.getContactEmail());
            ps.setBoolean(14,ugcAccount.isContactEmailConcent());

            rowsInserted = ps.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("UserGlobalCredentialsDAO:insertUserGlobalCredentialsData():): try" + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DAOException("UserGlobalCredentialsDAO:insertUserGlobalCredentialsData():: finally" + e.getMessage());
            }
        }
        return (rowsInserted > 0);        
    }

	/**
	 *
	 * @param ugcAccount
	 * @return
	 * @throws DAOException
	 */
	@Override
    public boolean removeUserGlobalCredentialsData(UserGlobalCredentials ugcAccount) throws DAOException {

		int rowsDeleted = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;		
		try {
			if (ugcAccount == null) return false;
			{ 
				con = getConnection();
				if (con == null) return false;				
                String query = "DELETE FROM global_credentials WHERE userID = ?";
                ps = con.prepareStatement(query);
                ps.setString(1, ugcAccount.getUserID());
                rowsDeleted = ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DAOException("UserGlobalCredentialsDAO:removeUserGlobalCredentialsData(): try" + e.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    freeConnection(con);
                }
            } catch (SQLException e) {
                throw new DAOException("UserGlobalCredentialsDAO:removeUserGlobalCredentialsData(): finally" + e.getMessage());
            }
        }        
        return (rowsDeleted > 0);
    }
}

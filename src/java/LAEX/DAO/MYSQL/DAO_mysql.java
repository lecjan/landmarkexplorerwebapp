/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.DAO.MYSQL;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Lech Jankowski
 */
public class DAO_mysql {

	/**
	 * Connection with mySQL databalse
	 * @return Connection
	 */
	public Connection getConnection(){

        String driver = "com.mysql.jdbc.Driver";
		/****************************************************************************************
		* connecting to remote server: dkit.hosting.acm.org port 3306
		*
		* database: dkithost_D00161848_landmark
		* username: dkithost_lech
		* password: jankowski67
		* 
		* connecting to localhost server: localhost:3306
		*
		* database: D00161848_landmark
		* username: root
		* password: <empty>		* 
		* 
		* 
		*****************************************************************************************/
		
        String url = "jdbc:mysql://localhost:3306/D00161848_landmark"; 
        String username = "root";
        String password = "";
		
        Connection con = null;
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException ex1) {
            System.out.println("Failed to find driver class " + ex1.getMessage());
            System.exit(1);
        } catch (SQLException ex2) {
            System.out.println("Connection failed " + ex2.getMessage());
            System.exit(2);
        }
        return con;
    }

	/**
	 *
	 * @param con
	 */
	public void freeConnection(Connection con){
        try {
            if (con != null) {
                con.close();
                con = null;
            }
        } catch (SQLException e) {
            System.out.println("Failed to free connection: " + e.getMessage());
            System.exit(1);
        }
    }
}

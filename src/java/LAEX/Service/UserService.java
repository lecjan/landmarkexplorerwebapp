/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Service;

import LAEX.Messages.AppWarningMessage;
import LAEX.Creational.Factory.UserFactory;
import LAEX.DAO.MYSQL.DAO_global_credentials;
import LAEX.Exceptions.DAOException;
import LAEX.DAO.MYSQL.DAO_app_user;
import LAEX.DAO.MYSQL.DAO_user_apps;
import LAEX.Business.CustomDataType.AccountPrivilege;
import LAEX.Business.CustomDataType.AccountType;
import LAEX.Business.User.User;
import LAEX.Business.UserApplication;
import LAEX.Business.UserGlobalCredentials;
import static LAEX.Security.Digest.generateHexSalt;
import static LAEX.Security.Digest.stringToHashSHA256;
import LAEX.Servlet.ActionServlet;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;

// DO NOT FORGET TO DELETE STATIC PHRASE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1

/**
 * Methods to process User objects from MySQL d00161848_landmark db
 * @author Lech Jankowski
 */
public class UserService {

	/**
	 * Login user with given credentials
	 * @param userName
	 * @param passWord
	 * @return User object or null if failed
	 */
	public User login(String userName, String passWord) {
		
		// Resetting warning message text used for Front-end
		AppWarningMessage.setText(""); 		

		UserGlobalCredentials ugc = null;
		UserApplication ua = null;
		User usr = null;
		
        DAO_global_credentials ugcDAO = new DAO_global_credentials();
		DAO_user_apps usrAppsDAO = new DAO_user_apps();	
		DAO_app_user usrDAO = new DAO_app_user();			

		/* 1 **************************************************************************************/
		// read record for user with a given username
		try {
			ugc = ugcDAO.selectByUsername(userName);
			if (ugc != null) System.out.println("1.UGC - login result selectByUsername         : " + ugc.toString());

		} catch (DAOException ex) {
			Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
		}
		if (ugc == null) { AppWarningMessage.setText("Invalid username!"); return usr; }
		
		if (ugc != null)
		{
			/* 2 **********************************************************************************/ 
			// If ugc <> null username has been found.
			// Now we must get check his password.
			// Read record again but this time with retrieved in first place - userID (which is the Salt) and password
			try {
				ugc = ugcDAO.selectByUserID(ugc.getUserID());
				if (ugc != null) System.out.println("2.UGC - login result selectByIDPassword       : " + ugc.toString());

				if (ugc != null)
				{
					
					/* 3 */
					// if ugc <> null user with given userID and password exists.
					// User authenticated !!!
					// Now we must check is the user authorised (has an account) and bound with LandmarkExplorer Application
					
					try {
						ua = usrAppsDAO.selectByIDPassword(ugc.getUserID(),passWord);
						if (ua != null) System.out.println("3.UA - login result selectByID                : " + ua.toString());

					} catch (DAOException ex) {
						Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
					}
					if (ua == null) { AppWarningMessage.setText("There is no application account for such credentials!"); return usr; }
					if (ua != null)
					{
						/* 4 **********************************************************************/ 
						// if ua <> null user is bound with the Application.
						// User authorised !!!
						// Now we must retrieve user for the Application

						try {
							usr = usrDAO.selectByGlobalCredentials(ugc);
							if (usr != null) System.out.println("4.USR - login result selectByGlobalCredentials: " + usr.toString());

							/* 5 **************************************************************/
							// LAST OPERATION TO PERFORM IS TO SET LOGIN STATUS:
							// Update the row in app_user table
							//

							try { 
								usr = usrDAO.updateLoginON(usr);
								if (usr != null) System.out.println("5.USR - login result updateLoginStatus: " + usr.toString());

							} catch (DAOException ex) {
								Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
							}
							if (usr == null) { AppWarningMessage.setText("You are already signed in from different session!"); return usr; }


						} catch (DAOException ex) {
							Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
						}
						if (usr == null) { AppWarningMessage.setText("Something went wrong.. please try again later!");  return usr; }
						
					}
					
					
				}
			} catch (DAOException ex) {
				Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		if (ugc == null) { 
			AppWarningMessage.setText("Invalid Username and/or Password!"); return usr; 
		}
		if (usr != null) { 
			AppWarningMessage.setText("Login succesfull !!!");
		}	
		// Here usr object must be correct (not null) and logged in (updated login status) 
		// or 
		// if (null) there is no such user credentials, association with application;
		return usr;
    }
	
	/**
	 * Check if username exists in db
	 * @param username
	 * @return true if yes, false if doesn't
	 */
	public boolean hasUser(String username) {
		AppWarningMessage.setText(""); 
        
		DAO_app_user uaDAO = new DAO_app_user();
        try {
            if (uaDAO.hasUsername(username)) return true; 
        } catch (DAOException e) {
            System.err.println("UserService:checkUser: " + e.getMessage());
        }
        return false;
    }
	
	/*
	public static User fetchData(String username) {
		AppWarningMessage.setText(""); 
        User usrTmp = null;
		DAOapp_user uaDAO = new DAOapp_user();
        try {
            if (uaDAO.hasUsername(username)) 
			{
				
				return true;
			} 
        } catch (DAOException e) {
            System.err.println("UserService:checkUser: " + e.getMessage());
        }
        return false;
    }	
	*/

	/**
	 * Logout User
	 * @param userName usernamke
	 * @return User object removed or null if failed to logout
	 */
	
	
	    public User logout(String userName) {
		// Resetting warning message text used for Front-end
		AppWarningMessage.setText(""); 		

		DAO_app_user usrDAO = new DAO_app_user();			
		
		User usr = null; // fresh read of user record of given userID
		
		try { 
			usr = usrDAO.selectByUsername(userName);
			System.out.println("loging out: "+usr.toString());
			
			if (usr != null) System.out.println("user before logout: " + usr.toString());
		} catch (DAOException ex) {
			Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		try { 
			usr = usrDAO.updateLoginOFF(usr);
			if (usr != null) System.out.println("user after logout: " + usr.toString());
		} catch (DAOException ex) {
			Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
		}
		
		if (usr == null) { AppWarningMessage.setText("Logout failure!"); return usr; }
		if (!usr.isLoggedIn()) { 
			AppWarningMessage.setText("Logged out succesfully !!!");
			System.out.println(AppWarningMessage.getText());
		}	
		return usr;
    }
	
	/**
	 * Creates and inserts new User into db
	 * @param userName username
	 * @param passWord password (plain)
	 * @param firstName first name
	 * @param lastName last name
	 * @param addr_line1 
	 * @param addr_line2
	 * @param addr_line3
	 * @param city
	 * @param region
	 * @param country
	 * @param phone
	 * @param ph_consent true if user consent for phone contact
	 * @param email
	 * @param em_consent true if user consent for email contact
	 * @param accType type of user account (private,commercial, administrator)
	 * @param accPrivilege type of user privilege (basic,premium,supervisor)
	 * @return
	 */
	public User addUser(String userName, String passWord, String firstName, String lastName, String addr_line1, String addr_line2, String addr_line3, String city, String region, String country, String phone, boolean ph_consent, String email, boolean em_consent, AccountType accType, AccountPrivilege accPrivilege) {
        boolean added = false;
		User newUser = null;

        if (userName != null && passWord != null && firstName != null && lastName != null && addr_line1 != null && addr_line2 != null && addr_line3 != null && city != null && region != null && country != null && phone != null && email != null) {
            String saltedEncryptedPassword = null;

			// insert crypto
			String userSaltAndID = generateHexSalt();
// insert check if userSaltAndID is already used if global_credentials (TO DO)			
			System.out.println("pass before salting:"+passWord);
			String newPasswordWithSalt = stringToHashSHA256(userSaltAndID+passWord);			
			System.out.println("pass after  salting:"+newPasswordWithSalt);

			DAO_global_credentials ugcDAO = new DAO_global_credentials();			
			DAO_user_apps uaDAO = new DAO_user_apps();
			DAO_app_user auDAO = new DAO_app_user();

			// 1st create new record in global_credentials
            UserGlobalCredentials ugc = new UserGlobalCredentials(userSaltAndID, userName, firstName, lastName, addr_line1, addr_line2, addr_line3, city, region, country, phone, ph_consent, email, em_consent);
            try {
                added = ugcDAO.insertUserGlobalCredentialsData(ugc);
// add check of application key from the server code is same as in application table-must be added into separate db				
				// 2nd add this application to user_apps records
				UserApplication ua = new UserApplication(userSaltAndID, newPasswordWithSalt, "LAEX"); // this application name is hardcoded with purpose
				try {
					System.out.println(ua.toString());
	                added = uaDAO.insert(ua);	
					
					// 3rd and last - add a new record for a user withing the application
					User au = UserFactory.createUserAccount(userSaltAndID, userName, accType, accPrivilege, null, null, false);
					try {
						added = auDAO.insert(au);
					} catch (DAOException ex) {
						 added = false;
						 Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
					}				
				} catch (DAOException ex) {
					 added = false;
					 Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
	            }					
            } catch (DAOException ex) {
				added = false;
                Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
			// extra verification was user added then must be read
			try {			
				newUser = auDAO.selectByUsername(userName);
			} catch (DAOException ex) {
				Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
			}
        }

        return newUser;
    }

	/**
	 * Retrieves a list of all User type users
	 * @return ArrayList of a User objects
	 */
	public ArrayList<User> getUsersAll() {
        DAO_app_user userDAO = new DAO_app_user();
        ArrayList<User> userList = new ArrayList<User>();

        try {
            userList = userDAO.viewUsersAll();
        } catch (DAOException ex) {
            Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return userList;
    }		
	
	/**
	 * Retrieves a list of administrator type users
	 * @return ArrayList of User
	 */
	public ArrayList<User> getUsersAdministrator() {
        DAO_app_user userDAO = new DAO_app_user();
        ArrayList<User> userList = new ArrayList<User>();

        try {
            userList = userDAO.viewUsersOfType(AccountType.ADMINISTRATOR);
        } catch (DAOException ex) {
            Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return userList;
    }			
	
	/**
	 * Retrieves a list of commercial type users
	 * @return ArrayList of User
	 */
	public ArrayList<User> getUsersCommercial() {
        DAO_app_user userDAO = new DAO_app_user();
        ArrayList<User> userList = new ArrayList<User>();

        try {
            userList = userDAO.viewUsersOfType(AccountType.COMMERCIAL);
        } catch (DAOException ex) {
            Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return userList;
    }			
	
	/**
	 * Retrieves a list of private type users
	 * @return ArrayList of Users
	 */
	public ArrayList<User> getUsersPrivate() {
        DAO_app_user userDAO = new DAO_app_user();
        ArrayList<User> userList = new ArrayList<User>();

        try {
            userList = userDAO.viewUsersOfType(AccountType.PRIVATE);
        } catch (DAOException ex) {
            Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return userList;
    }					
	
	/**
	 * tests
	 * @param args
	 */
	public static void main(String[] args) {
		/*
		User usr = UserService.login("lecjan","lecjan");
		if (usr != null) System.out.println("FINAL:usr: "+usr.toString());		
		System.out.println("LOGIN:msg: "+AppWarningMessage.getText());	
		
		System.out.println("has lecjan: "+hasUser("lecjan"));	
		System.out.println("has lecjan1: "+hasUser("lecjan1"));		
		
		UserService.logout("lecjan");
		if (usr != null) System.out.println("LOGOUT:usr: "+usr.toString());	

		*/
		UserService us = new UserService();
		User usr2add = null;

		usr2add  = us.addUser("adminBasic", "adminBasic5", "Lech", "Jankowski", "Paderewskiego 4", "Suchanino", "80-169", "Gdansk", "pomorskie", "Poland", "0857870045", true, "lecjan@wp.pl", true, AccountType.ADMINISTRATOR, AccountPrivilege.BASIC);
		if (usr2add != null) System.out.println("insert application user: "+usr2add.toString());		

		usr2add  = us.addUser("adminPremium", "adminPremium6", "Lech", "Jankowski", "Paderewskiego 4", "Suchanino", "80-169", "Gdansk", "pomorskie", "Poland", "0857870045", true, "lecjan@wp.pl", true, AccountType.ADMINISTRATOR, AccountPrivilege.PREMIUM);
		if (usr2add != null) System.out.println("insert application user: "+usr2add.toString());		

		usr2add  = us.addUser("adminSuper", "adminSuper7", "Lech", "Jankowski", "Paderewskiego 4", "Suchanino", "80-169", "Gdansk", "pomorskie", "Poland", "0857870045", true, "lecjan@wp.pl", true, AccountType.ADMINISTRATOR, AccountPrivilege.SUPERVISOR);
		if (usr2add != null) System.out.println("insert application user: "+usr2add.toString());		
		
		
    }		
			
	
}

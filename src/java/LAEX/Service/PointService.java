/*
 * Copyright (C) 2015 Lech Jankowski
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package LAEX.Service;


import LAEX.Business.MapPoint;
import LAEX.DAO.NEO4J.DAO_neo4j;
import LAEX.Exceptions.DAOException;
import LAEX.Servlet.ActionServlet;

import java.sql.SQLException;
import java.util.ArrayList;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Services to process node/point of a graph database neo4j
 * @author Ledch Jankowski
 */
public class PointService {

	/**
	 * Gets ArrayList of all MapPoints
	 * @return ArrayList of MapPoint types
	 */
	public ArrayList<MapPoint> getPointsAll() {
        DAO_neo4j graphDAO = new DAO_neo4j();
        ArrayList<MapPoint> pointList = new ArrayList<MapPoint>();

        try {
            pointList = graphDAO.viewPointsAll();
        } catch (DAOException ex) {
            Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
			Logger.getLogger(PointService.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(PointService.class.getName()).log(Level.SEVERE, null, ex);
		}
        return pointList;
    }			
	
	/**
	 * Creates new MapPoint
	 * @param aPoint
	 * @return rows affected
	 */
	public int createPoint(MapPoint aPoint) {
		int rows = -1;
        DAO_neo4j graphDAO = new DAO_neo4j();
		
		// get maximum value of pointID from graph database
		int maxPointID = -1;
		try {
			maxPointID = graphDAO.getMaxPointID();
		} catch (SQLException ex) {
			Logger.getLogger(PointService.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(PointService.class.getName()).log(Level.SEVERE, null, ex);
		}
		// set new point's pointID as increment of 1 of max value got before
		aPoint.setPointID(maxPointID + 1);
        try {
            graphDAO.createPointNode(aPoint);
			// dummy count of affected rows - if here creation was syccesful so 1 row has been affected;
			rows = 1;
        } catch (DAOException ex) {
            Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) { 
			Logger.getLogger(PointService.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(PointService.class.getName()).log(Level.SEVERE, null, ex);
		} 
        return rows;
    }		
	
	/**
	 * Updates MapPoint
	 * @param aPoint
	 * @return rows affected
	 */
	public int updatePoint(MapPoint aPoint) {
		int rows;
        DAO_neo4j graphDAO = new DAO_neo4j();
		MapPoint point = null;
		System.out.println("!!!!! updatePoint:"+aPoint.toString());
        try {
            point = graphDAO.setPointProperty(aPoint, "name", aPoint.getName());
            point = graphDAO.setPointProperty(aPoint, "description", aPoint.getDescription());
            point = graphDAO.setPointProperty(aPoint, "type", aPoint.getType().toString());
			point = graphDAO.setPointProperty(aPoint, "geoLat", aPoint.getGeoLocation().getLatitude());
            point = graphDAO.setPointProperty(aPoint, "geoLon", aPoint.getGeoLocation().getLongitude());

        } catch (DAOException ex) {
            Logger.getLogger(ActionServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) { 
			Logger.getLogger(PointService.class.getName()).log(Level.SEVERE, null, ex);
		} catch (SQLException ex) {
			Logger.getLogger(PointService.class.getName()).log(Level.SEVERE, null, ex);
		} 
		if (point != null) rows = 1; 
		else rows = -1;
        return rows;
    }		
	
	/**
	 * Retrieves MapPoint from db of a given id
	 * @param pointID
	 * @return MapPoint
	 */
	public MapPoint getPoint(int pointID) {
		DAO_neo4j graphDAO = new DAO_neo4j();
		MapPoint point = null;
		try {
			point = graphDAO.selectPointById(pointID);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(PointService.class.getName()).log(Level.SEVERE, null, ex);
		} catch (SQLException ex) {
			Logger.getLogger(PointService.class.getName()).log(Level.SEVERE, null, ex);
		}
		return point;
	}			
	
	/**
	 * Removes MapPoint from db of a given id
	 * @param pointID
	 * @return rows affected
	 */
	public int deletePoint(int pointID) {
		DAO_neo4j graphDAO = new DAO_neo4j();
		int rows = -1;
		try {
			rows = graphDAO.deletePointById(pointID);
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(PointService.class.getName()).log(Level.SEVERE, null, ex);
		} catch (SQLException ex) {
			Logger.getLogger(PointService.class.getName()).log(Level.SEVERE, null, ex);
		}
		return rows;
	}			
	
}

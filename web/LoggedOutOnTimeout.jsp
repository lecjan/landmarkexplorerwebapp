<%@page import="LAEX.Service.UserService"%>
<%@page import="LAEX.Command.Factory.UserLogout"%>
<%@page import="LAEX.Servlet.ActionServlet"%>
<%@page import="LAEX.Messages.AppWarningMessage"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- TEMPLATE saved from url=(0040)http://getbootstrap.com/examples/signin/ -->
<html lang="en">
    <head>
        <link rel="icon" href="favicon.ico">
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Landmark Explorer</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

        <!-- Custom styles for this template -->
        <link href="css/landmark-admin.css" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet" >

        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script type="text/javascript">
            window.alert = function(){};
            var defaultCSS = document.getElementById('bootstrap-css');
            function changeCSS(css){
                if(css) $('head > link').filter(':first').replaceWith('<link rel="stylesheet" href="'+ css +'" type="text/css" />'); 
                else $('head > link').filter(':first').replaceWith(defaultCSS); 
            }
            $( document ).ready(function() {
              var iframe_height = parseInt($('html').height()); 
            });
        </script>    

    </head>
    <body>
        <div class="container">    
            <center>
            <h1>You have been loggged out due to session inactivity</h1>

            <!-- reading "old" username to clear login status in DB -->
            <!--
            <script type="text/javascript">
                var client = localStorage.getItem('user');
            </script>    
            -->
            
            <% 
                //UserService us = new UserService();
                AppWarningMessage.setText("Session has timed out due to inactivity..."); 
            %>         
            <form id="form_link_to_action_servlet" method="post" action="ActionServlet">
                <li>
                    <h2><a href="javascript:;" onclick="document.getElementById('form_link_to_action_servlet').submit();">Return to Loggin Page here...</a></h2>
                    <input type="hidden" name="action" value="gotoLandingPage"/>
                </li>
            </form>             
            <center>
        </div>  
    </body>
</html>
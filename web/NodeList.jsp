<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page import="LAEX.Service.PointService"%>
<%@page import="LAEX.Business.CustomDataType.PointType"%>
<%@page import="LAEX.Business.MapPoint"%>
<%@page import="LAEX.Business.User.UserCommercialPremium"%>
<%@page import="LAEX.Business.User.UserCommercialBasic"%>
<%@page import="LAEX.Business.User.UserPrivatePremium"%>
<%@page import="LAEX.Business.User.UserPrivateBasic"%>
<%@page import="java.util.ArrayList"%>
<%@page import="LAEX.Business.User.User"%>


<html lang="en">

<head>
    <!-- dealing with timeout -->
    <meta HTTP-EQUIV="refresh" CONTENT="<%= session.getMaxInactiveInterval()%>; URL=LoggedOutOnTimeout.jsp" />
    
    <link rel="icon" href="favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Landmark Explorer</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/landmark-admin.css" rel="stylesheet">
    
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Custom DataTablesCSS -->
    <link href="css/jquery.dataTables-1.10.5.min.css" rel="stylesheet">
    <link href="css/dataTables.bootstrap.css" rel="stylesheet">

    <script type='text/javascript' src="http://maps.google.com/maps/api/js?sensor=true&.js"></script>
    
    <script type='text/javascript' src="js/googlemap-laex-click.js"> </script>
    
</head>

<body>

    <!-- LJ *********************************************************-->
    <% User usr = (User) (request.getSession().getAttribute("user")); %>
    <!-- LJ *********************************************************-->
    
    <!-- declaring current Point object to be recogised and processed by the forms -->
    <% MapPoint currentPoint = null; %>   
    
    <c:choose>
        <c:when test="${user.type=='ADMINISTRATOR'}">        
    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="HomeAdmin.jsp">LandmarkExplorer</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="navbar-text">
                            <p>Account Type: <span class="label label-default"><% out.print(usr.getType()); %></span> <span class="label label-default"><% out.print(usr.getPrivilege()); %></span></p>                </li>                
                <!-- USER MENU -->
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>
                        <!-- LJ *********************************************************-->
                        <% out.print(usr.getUserName()); %>
                        <!-- LJ *********************************************************-->
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                            <form id="user_menu_logout" method="post" action="ActionServlet">
                                <li>
                                    <!--
                                <a href="javascript:;" onclick="document.getElementById('user_menu_logout').submit();"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                                    -->
                                <a href="javascript:;" onclick="document.getElementById('user_menu_logout').submit();"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                                <input type="hidden" name="action" value="Logout"/>
                                <a <% out.print("E"+usr.getUserName()); %>></a> 
                                </li>
                            </form>                            
                    </ul>
                </li>
            </ul>
                                
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <!--<ul class="nav navbar-nav side-nav">-->
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="HomeAdmin.jsp"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#Users_dropdown"><i class="fa fa-fw fa-arrows-v"></i> Users <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="Users_dropdown" class="collapse">
                            <li>
                                <a href="UserList.jsp"><i class="fa fa-fw fa-table"></i> List</a>                                
                            </li>                             
                            <li>
                                <a href="UserAdd.jsp"><i class="fa fa-fw fa-table"></i> Add new User</a>                                
                            </li>     
                        </ul>
                    </li>
                    <li class="active">
                        <a href="javascript:;" data-toggle="collapse" data-target="#Places_dropdown" class="" aria-expanded="true"><i class="fa fa-fw fa-arrows-v"></i> Nodes <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="Places_dropdown" class="collapse in" aria-expanded="true">
                            <li>
                                <a href="NodeList.jsp"><i class="fa fa-fw fa-table"></i> List</a>                                
                            </li>                             
                            <li>
                                <a href="NodeEdit.jsp"><i class="fa fa-fw fa-table"></i> Add new Node</a>                                
                            </li>                             
                        </ul>
                    </li>           
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#Ads_dropdown"><i class="fa fa-fw fa-arrows-v"></i> Advertisements <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="Ads_dropdown" class="collapse">
                            <li>
                                <a href="AdList.jsp"><i class="fa fa-fw fa-table"></i> List</a>                                
                            </li>                             
                            <li>
                                <a href="AdAdd.jsp"><i class="fa fa-fw fa-table"></i> Add new Advert</a>                                
                            </li>                             
                        </ul>
                    </li>                     
                    <li>
                        <a href="DashTours.jsp"><i class="fa fa-fw fa-file"></i> Tours</a>
                    </li>
                    <li>
                        <a href="DashPaths.jsp"><i class="fa fa-fw fa-file"></i> Paths</a>
                    </li>    
                    <li>
                        <a href="DashTags.jsp"><i class="fa fa-fw fa-file"></i> Tags</a>
                    </li>  
                    <li>
                        <a href="Home-AtRight.jsp"><i class="fa fa-fw fa-dashboard"></i> RTL Dashboard</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Points
                        </h1>
                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-dashboard"></i>  <a href="HomeAdmin.jsp">Dashboard</a>
                            </li>
                            <li>
                                <i class="fa fa-arrows-v"></i> Points
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> All
                            </li>
                        </ol>
                    </div>
                </div> 
                <!-- /.row -->

                <div class="row"> 
                    <div class="col-lg-4">
                        <h2>Details</h2>                        
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Point</h3>
                            </div>
                            <div class="panel-body" style="height: 630px">
                                
                                <div id="pointPanelView">
                                    <form id="pointPanelForm" method="post" name="pointPanelForm" action="ActionServlet">
                                        <fieldset id="pointPanelFieldpointID" disabled>
                                        <div class="form-group">
                                            <label for="pointId">point ID</label>
                                            <input type="text" class="form-control" name="pointID" value="" id="pointId" placeholder="pointID is read only">
                                        </div>
                                        </fieldset>  
                                        <fieldset id="pointPanelFieldSet" disabled>                                        
                                        <div class="form-group">
                                            <label for="name">name</label>
                                            <input type="text" class="form-control" name="name" value="" id="name" placeholder="Enter name">
                                        </div>
                                        <div class="form-group">
                                            <label for="description">description</label>
                                            <textarea class="form-control" rows="2" cols="50" name="description" form="pointPanelForm" id="description" placeholder="Enter description"></textarea> 
                                        </div>
                                        <div class="form-group">
                                            <label for="type">type</label>
                                            <input type="text" class="form-control" name="type" value="" id="type" placeholder="Select type">
                                        </div>
                                        <div class="form-group">
                                            <label for="latitude">geo latitude</label>
                                            <input type="text" class="form-control" name="latitude" value="" id="latitude" placeholder="Enter latitude">
                                        </div>                                        
                                        <div class="form-group">
                                            <label for="longitude">geo longitude</label>
                                            <input type="text" class="form-control" name="longitude" value="" id="longitude" placeholder="Enter longitude">
                                        </div>       
                                        </fieldset>
                                        
                                        <input id="actionFromPointEdit" type="hidden" name="action" value="" id="submit_action"/>
                                        
                                        <c:choose>
                                            <c:when test="${user.updatePinPoints()==true}">
                                                <button type="button" class="btn btn-default" id="pointEditBtn" action="editPoint" value="Edit" onclick="editPointPanel();location.href='#'" disabled="false">Edit</button>           
                                                <button type="submit" class="btn btn-default" id="pointSaveBtn" action="updatePoint" value="Save" disabled="true">Save Changes</button>  
                                            </c:when>
                                            <c:otherwise>
                                                <button type="button" class="btn btn-default" id="pointEditBtn" action="editPoint" value="Edit" disabled="true">Edit</button>           
                                                <button type="submit" class="btn btn-default" id="pointSaveBtn" action="updatePoint" value="Save" disabled="true">Save Changes</button>  
                                            </c:otherwise>
                                        </c:choose>
                                        </br>
                                        <c:choose>
                                            <c:when test="${user.addPinPoints()==true}">
                                                <button type="button" class="btn btn-default" id="pointAddBtn" action="addPoint" value="Add New" onclick="addNewPointPanel();location.href='#'" disabled="false">Add New</button>                                                                          
                                                <button type="submit" class="btn btn-default" id="pointCreateBtn" action="createPoint" value="Save" disabled="true">Save Added</button>                                           
                                            </c:when>
                                            <c:otherwise>
                                                <button type="button" class="btn btn-default" id="pointAddBtn" action="addPoint" value="Add New" disabled="true">Add New</button>                                                                          
                                                <button type="submit" class="btn btn-default" id="pointCreateBtn" action="createPoint" value="Save" disabled="true">Save Added</button>                                           
                                            </c:otherwise>
                                        </c:choose>
                                        </br>
                                        <button type="button" class="btn btn-default" id="pointCancelBtn" action="cancelPoint" value="Cancel" onclick="clearPointPanel();location.href='#'" disabled="true">Cancel</button>                                                                          
                                    </form>      
                                </div>                                
                                <!-- end pointsPanelView -->
                                
                                <!-- Yes/No version 1-->
                                <!--
                                <div id="confirm" class="modal hide fade">
                                  <div class="modal-body">
                                    Are you sure?
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" data-dismiss="modal" class="btn btn-primary" id="delete">Delete</button>
                                    <button type="button" data-dismiss="modal" class="btn">Cancel</button>
                                  </div>
                                </div>
                                -->
                                <!-- end of Yes/No -->

                                <!-- Yes/No version 2-->
                                <!-- set up the modal to start hidden and fade in and out -->
                                <!--
                                <div id="modalRemove" class="modal fade" visibility="hidden">
                                  <div class="modal-dialog">
                                    <div class="modal-content">
                                      <div class="modal-body">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        Confirm remove action or close the dialog...
                                      </div>
                                      <div class="modal-footer"><button type="Submit" class="btn btn-default" value="Remove" onclick="removePointPanel()">Remove</button> </div>
                                    </div>
                                  </div>
                                </div>                                
                                -->
                            </div>
                        </div>                        
                    </div>
                    <!-- Table -->
                    <div class="col-md-4">
                        <h2>All Points</h2>
                        <div class="panel panel-default" >
                            <div class="panel-heading">
                                <h3 class="panel-title">Point</h3>
                            </div>
                            <div class="panel-body" style="height: 630px">                        
                                <div class="table table-responsive"  style="height: 600px">
                                    <table id="table-points-all" class="table table-hover table-striped"x">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th>Actions</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                ArrayList<MapPoint> pointList = new ArrayList<MapPoint>();
                                                PointService ps = new PointService();
                                                pointList = ps.getPointsAll();
                                                for (MapPoint p: pointList){ 
                                                    if (p != null)
                                                    {
                                                        session.setAttribute("sessionPoint", p);
                                            %>
                                            <tr onclick="selectPointPanel('<c:out value="${sessionPoint.pointID}"/>','<c:out value="${sessionPoint.name}"/>','<c:out value="${sessionPoint.description}"/>','<c:out value="${sessionPoint.type}"/>','<c:out value="${sessionPoint.geoLocation.latitude}"/>','<c:out value="${sessionPoint.geoLocation.longitude}"/>');" >
                                                <td><c:out value='${sessionPoint.pointID}'/></td>
                                                <td><c:out value='${sessionPoint.name}'/></td>
                                                <td><c:out value='${sessionPoint.type}'/></td>
                                                <c:choose><c:when test="${user.updatePinPoints()==true}"> <td><a href="ActionServlet?action=editPoint&pointID=<c:out value='${sessionPoint.pointID}'/>" class="glyphicon glyphicon-edit"></a></td> </c:when><c:otherwise><td>n/a</td></c:otherwise></c:choose>
                                                <c:choose><c:when test="${user.deletePinPoints()==true}"> <td><a href="ActionServlet?action=deletePoint&pointID=<c:out value='${sessionPoint.pointID}'/>" class="glyphicon glyphicon-trash"></a></td> </c:when><c:otherwise><td>n/a</td></c:otherwise></c:choose>
                                            <%      }
                                                } 
                                            %>                  
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>Id</th>
                                                <th>Name</th>
                                                <th>Type</th>
                                                <th>Actions</th>
                                            </tr>
                                        </tfoot>                                
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end Table -->
                                
                    <!-- Google maps goes here -->
                    <div class="col-md-4">
                        <h2>Area Map</h2>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title">Google Map</h3>
                            </div>
                            <div class="panel-body" style="height:630px;">
                                <div id="simpleMap" style="height:500px;"></div>
                                <center>
                                </br>
                                <input onclick="clearMarkers();" type=button value="Hide Markers">
                                <input onclick="showMarkers();" type=button value="Show All Markers">
                                <input onclick="deleteMarkers();" type=button value="Delete Markers">   
                                </br>
                                <p>Click on the map to add markers.</p>
                                <input id="markerLatLon" type=text value="Here'll be copied marker's coordinates" size='50'>
                                </center>
                            </div>
                        </div>
                    </div>
                    <!-- end google maps -->                                 
                                
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
        </c:when>
        <c:otherwise>
            <div id="wrapper">     
               <div id="page-wrapper">
                    <div class="container-fluid">            
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-info">
                                    <i class="fa fa-info-circle"></i>  <strong>You are not authorized to access this content</strong>
                                </div>
                            </div>
                        </div>            
                    </div>
               </div>
            </div>
        </c:otherwise>
    </c:choose>     

    <!-- jQuery -->
    <script src="js/jquery-1.11.1.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- jQuery DataTables-->
    <script src="js/jquery.dataTables-1.10.5.min.js"></script>
    <script src="js/dataTables.bootstrap.js"></script>
    
    <!-- Google Maps -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=weather"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCj20qCYRGPtOtNdrjAstzbt3N8-0TOLqg&callback=initMap" type="text/javascript"></script>    
    <!--<script src="assets/js/google-maps/googlemap-conf.js"></script>-->
    
    <!-- Landmark Unique JavaScripts -->
    <script src="js/LandmarkScripts.js"></script>    

    <!--
    <script>
    $(document).ready(function(){
        $('#table-points-all').DataTable();
    });     
    </script>
    -->
    
    <!-- pagination and size of a table -->
    
    <!--
    <script>
    $(document).ready(function(){
        $('#table-points-all').dataTable( {
                "scrollY":        "380px",
                "scrollCollapse": true,
                "paging":         true
            } );        
    });     
    </script>    
-->
    <!-- selecting row in the table -->
    <script>
    $(document).ready(function() {
        var table = $('#table-points-all').DataTable();

        $('#table-points-all tbody').on( 'click', 'tr', function () {
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');                
                var datarow = table.row('.selected').data();
            }
        } );
        $('#button').click( function () {
            table.row('.selected').remove().draw( false );
        } );
    } );
    </script>    
    
    <!-- sometime later, probably inside your on load event callback -->
    
    <!--
    <script>
    $(document).ready(function() {
  
        $("#modalRemove").on("show", function() {    // wire up the OK button to dismiss the modal when shown
            $("#modalRemove a.btn").on("click", function(e) {
                console.log("button pressed");   // just as an example...
                $("#modalRemove").modal('hide');     // dismiss the dialog
            });
        });

        $("#modalRemove").on("hide", function() {    // remove the event listeners when the dialog is dismissed
            $("#modalRemove a.btn").off("click");
        });

        $("#modalRemove").on("hidden", function() {  // remove the actual elements from the DOM when fully hidden
            $("#modalRemove").remove();
        });

        $("#modalRemove").modal({                    // wire up the actual modal functionality and show the dialog
          "backdrop"  : "static",
          "keyboard"  : true,
          "show"      : true                     // ensure the modal is shown immediately
        });
    } );    
    </script>    
    -->

</body>

</html>

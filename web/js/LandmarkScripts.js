( function () {
     // setting hidden action input type back to updatePoint
    document.getElementById("actionFromPointEdit").value = "updatePoint"; 
    // fieldsets visibility
    document.getElementById("pointPanelFieldSet").disabled = true;    
    document.getElementById("pointPanelFieldpointID").disabled = true;     
    // buttons visibility
    document.getElementById("pointEditBtn").disabled = false; 
    document.getElementById("pointAddBtn").disabled = false;   
    document.getElementById("pointSaveBtn").disabled = true; 
    document.getElementById("pointCancelBtn").disabled = true;     
    document.getElementById("pointCreateBtn").disabled = true;  
})();

function selectPointPanel(a,b,c,d,e,f) {
    // setting hidden action input type back to updatePoint
    document.getElementById("actionFromPointEdit").value = "updatePoint"; 
    // fieldsets visibility
    document.getElementById("pointPanelFieldSet").disabled = true; 
    document.getElementById("pointPanelFieldpointID").disabled = true;     
    // buttons visibility
    document.getElementById("pointEditBtn").disabled = false; 
    document.getElementById("pointAddBtn").disabled = false;   
    document.getElementById("pointSaveBtn").disabled = true; 
    document.getElementById("pointCancelBtn").disabled = true;     
    document.getElementById("pointCreateBtn").disabled = true;  
    // assigning values into a form
    document.getElementById("pointId").value = a;
    document.getElementById("name").value = b;    
    document.getElementById("description").value = c;
    document.getElementById("type").value = d;
    document.getElementById("latitude").value = e;
    document.getElementById("longitude").value = f;
}   

function editPointPanel() {
    // setting hidden action input type back to updatePoint
    document.getElementById("actionFromPointEdit").value = "updatePoint"; 
    // fieldsets visibility
    document.getElementById("pointPanelFieldSet").disabled = false; 
    document.getElementById("pointPanelFieldpointID").disabled = false;     
    // buttons visibility
    document.getElementById("pointEditBtn").disabled = true; 
    document.getElementById("pointAddBtn").disabled = true;      
    document.getElementById("pointSaveBtn").disabled = false; 
    document.getElementById("pointCancelBtn").disabled = false;     
    document.getElementById("pointCreateBtn").disabled = true;  
}

function addNewPointPanel() {
    // setting hidden action input type to createPoint
    document.getElementById("actionFromPointEdit").value = "createPoint";       
    // fieldsets visibility
    document.getElementById("pointPanelFieldSet").disabled = false; 
    document.getElementById("pointPanelFieldpointID").disabled = true;     
    // buttons visibility
    document.getElementById("pointEditBtn").disabled = true; 
    document.getElementById("pointAddBtn").disabled = true;      
    document.getElementById("pointSaveBtn").disabled = true; 
    document.getElementById("pointCancelBtn").disabled = false;     
    document.getElementById("pointCreateBtn").disabled = false;  
    // blanking off the values
    document.getElementById("pointID").value = "a";
    document.getElementById("name").value = "bhgfjhfgjhgfh";    
    document.getElementById("description").value = "c";
    document.getElementById("type").value = "d";
    document.getElementById("latitude").value = "e";
    document.getElementById("longitude").value = "f";    
}

function clearPointPanel(a,b,c,d,e,f) {
    // setting hidden action input type back to updatePoint
    document.getElementById("actionFromPointEdit").value = "updatePoint";  
    // fieldsets visibility
    document.getElementById("pointPanelFieldSet").disabled = true; 
    document.getElementById("pointPanelFieldpointID").disabled = true;     
    // buttons visibility
    document.getElementById("pointEditBtn").disabled = false; 
    document.getElementById("pointAddBtn").disabled = false;    
    document.getElementById("pointSaveBtn").disabled = true; 
    document.getElementById("pointCancelBtn").disabled = true;     
    document.getElementById("pointCreateBtn").disabled = true;     
    // restoring the values the values
    document.getElementById("pointID").value = a;
    document.getElementById("name").value = b;    
    document.getElementById("description").value = c;
    document.getElementById("type").value = d;
    document.getElementById("latitude").value = e;
    document.getElementById("longitude").value = f;
}   


// that may be helpfull with logging off from db on session timeout
function saveUsernameLocaly()
{
    var localUsername = localStorage.getItem("client");

    if (storeddata !== null)
    {
        localStorage.setItem("BOX2COLOR",x);
        document.getElementById("box2").style.backgroundColor = x;
        document.getElementById("box1").style.backgroundColor = x;
    } else
    {
        localStorage.setItem("BOX2COLOR",x);
        document.getElementById("box2").style.backgroundColor = x;
        document.getElementById("box1").style.backgroundColor = x;
    }
    
}

function restoreBox2()
{
    var storeddata = localStorage.getItem("BOX2COLOR");
    var x = document.forms[0]["fcolor"].value;
    if (storeddata !== null)
    {
        document.getElementById("box2").style.backgroundColor = storeddata;
    }     
}

// below ones unused


function loginDisplay(loginType) {
    console.log("in login display");                        
    document.getElementById("submit_login").value = loginType;
    document.getElementById("submit_action").value = loginType;
    document.getElementById("login_display").style.height = "60px";
    document.getElementById("login_display").style.visibility = "visible";
    document.getElementById("login_buttons").style.visibility = "collapse";
    document.getElementById("login_buttons").style.height = "0px";   
    console.log("login type set to:"+document.getElementById("submit_login").value);
}

function loginCancel() {
    console.log("in login cancel");
    document.getElementById("usernameText").value = "";
    document.getElementById("passwordText").value = "";
    document.getElementById("login_buttons").style.height = "60px";
    document.getElementById("login_buttons").style.visibility = "visible";                         
    document.getElementById("login_display").style.visibility = "collapse";
    document.getElementById("login_display").style.height = "0px";
}   

function registrationDisplay() {
    console.log("in registration display");                        
    document.getElementById("login_display").style.height = "200px";
    document.getElementById("login_display").style.visibility = "visible";
    document.getElementById("login_buttons").style.visibility = "visible";
    document.getElementById("login_buttons").style.height = "60px";   
}

function registrationCancel() {
    console.log("in registration cancel");
    document.getElementById("submit_action").value = "notset";
    document.getElementById("login_buttons").style.height = "60px";
    document.getElementById("login_buttons").style.visibility = "visible";                         
    document.getElementById("login_display").style.visibility = "collapse";
    document.getElementById("login_display").style.height = "0px";
}   

function orderBook(a,b,c,d,e) {
    console.log("in order book");
    document.getElementById("single_order").style.height = "200px";
    document.getElementById("single_order").style.visibility = "visible";    
    document.getElementById("bookIdText").value = a;
    document.getElementById("titleText").value = b;
    document.getElementById("authorText").value = c;
    document.getElementById("genreText").value = d;
    document.getElementById("priceText").value = e;
}   

function orderBookCancel() {
    console.log("in order book cancel");
    document.getElementById("bookIdText").value = "";
    document.getElementById("titleText").value = "";
    document.getElementById("authorText").value = "";
    document.getElementById("genreText").value = "";
    document.getElementById("priceText").value = "";
    document.getElementById("single_order").style.visibility = "collapse";
    document.getElementById("single_order").style.height = "0px";    
}   

// Javascript validation used if HTML5 validation not supported
function validateLogin()
{
        var user=document.getElementById("usernameText").value;
        user=user.trim();
        var pass=document.getElementById("passwordText").value;
        pass=pass.trim();                            
        if(user === null)
        {
                document.getElementById('error').innerHTML="Please Enter Username";
                return false;
        } else {
            /* provission for Javascript validation in HTML5, CSS3 generic validation is not supported by browser */
        }

        if(pass === null)
        {
                document.getElementById('error').innerHTML="Please Enter Password";
                return false;
        } else {
            /* provission for Javascript validation in HTML5, CSS3 generic validation is not supported by browser */
        }
        return true;                           
}     

function validateRegistration()
{
    // to be amended to suit registration - may not be used.....
        var user=document.getElementById("usernameText").value;
        user=user.trim();
        var pass=document.getElementById("passwordText").value;
        pass=pass.trim();                            
        if(user === null)
        {
                document.getElementById('error').innerHTML="Please Enter Username";
                return false;
        } else {
            /* provission for Javascript validation in HTML5, CSS3 generic validation is not supported by browser */
        }

        if(pass === null)
        {
                document.getElementById('error').innerHTML="Please Enter Password";
                return false;
        } else {
            /* provission for Javascript validation in HTML5, CSS3 generic validation is not supported by browser */
        }
        return true;                           
} 

function editDetails() {
    document.getElementById("fNameDetails").readOnly = false;
    document.getElementById("sNameDetails").readOnly = false;
    document.getElementById("phoneDetails").readOnly = false;
    document.getElementById("address1Details").readOnly = false;
    document.getElementById("address2Details").readOnly = false;
    document.getElementById("cityDetails").readOnly = false;
    document.getElementById("countryDetails").readOnly = false;
    document.getElementById("edit_details").style.visibility = "collapse";
    document.getElementById("save").style.visibility = "visible";
}




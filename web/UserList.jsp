<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="LAEX.Business.User.UserCommercialPremium"%>
<%@page import="LAEX.Business.User.UserCommercialBasic"%>
<%@page import="LAEX.Business.User.UserPrivatePremium"%>
<%@page import="LAEX.Business.User.UserPrivateBasic"%>
<%@page import="LAEX.Service.UserService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="LAEX.Business.User.User"%>


<html lang="en">

<head>
    <!-- dealing with timeout -->
    <meta HTTP-EQUIV="refresh" CONTENT="<%= session.getMaxInactiveInterval()%>; URL=LoggedOutOnTimeout.jsp" />
        
    <link rel="icon" href="favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Landmark Explorer</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/landmark-admin.css" rel="stylesheet">
    
    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Custom DataTablesCSS -->
    <link href="css/jquery.dataTables-1.10.5.min.css" rel="stylesheet">
    <link href="css/dataTables.bootstrap.css" rel="stylesheet">

</head>

<body>

    <!-- LJ *********************************************************-->
    <% User usr = (User) (request.getSession().getAttribute("user")); %>
    <!-- LJ *********************************************************-->
    <c:choose>
        <c:when test="${user.type=='ADMINISTRATOR'}">  
            <div id="wrapper">

                <!-- Navigation -->
                <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="HomeAdmin.jsp">LandmarkExplorer</a>
                    </div>
                    <!-- Top Menu Items -->
                    <ul class="nav navbar-right top-nav">
                        <li class="navbar-text">
                            <p>Account Type: <span class="label label-default"><% out.print(usr.getType()); %></span> <span class="label label-default"><% out.print(usr.getPrivilege()); %></span></p>
                        </li>                        
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>
                                <!-- LJ *********************************************************-->
                                <% out.print(usr.getUserName()); %>
                                <!-- LJ *********************************************************-->
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                                </li>
                                <li class="divider"></li>


                                    <form id="user_menu_logout" method="post" action="ActionServlet">
                                        <li>
                                            <!--
                                        <a href="javascript:;" onclick="document.getElementById('user_menu_logout').submit();"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                                            -->
                                        <a href="javascript:;" onclick="document.getElementById('user_menu_logout').submit();"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                                        <input type="hidden" name="action" value="Logout"/>
                                        <a <% out.print("E"+usr.getUserName()); %>></a> 
                                        </li>
                                    </form>                            


                            </ul>
                        </li>
                    </ul>

                    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <!--<ul class="nav navbar-nav side-nav">-->
                        <ul class="nav navbar-nav side-nav">
                            <li>
                                <a href="HomeAdmin.jsp"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                            </li>
                            <li class="active">
                                <a href="javascript:;" data-toggle="collapse" data-target="#Users_dropdown" class="" aria-expanded="true"><i class="fa fa-fw fa-arrows-v"></i> Users <i class="fa fa-fw fa-caret-down"></i></a>
                                <ul id="Users_dropdown" class="collapse in" aria-expanded="true">
                                    <li>
                                        <a href="UserList.jsp"><i class="fa fa-fw fa-table"></i> List</a>                                
                                    </li>                             
                                    <li>
                                        <a href="UserAdd.jsp"><i class="fa fa-fw fa-table"></i> Add new</a>                                
                                    </li>                             
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:;" data-toggle="collapse" data-target="#Places_dropdown"><i class="fa fa-fw fa-arrows-v"></i> Nodes <i class="fa fa-fw fa-caret-down"></i></a>
                                <ul id="Places_dropdown" class="collapse">
                                    <li>
                                        <a href="NodeList.jsp"><i class="fa fa-fw fa-table"></i> List</a>                                
                                    </li>                             
                                    <li>
                                        <a href="NodeAdd.jsp"><i class="fa fa-fw fa-table"></i> Add new</a>                                
                                    </li>                             
                                </ul>
                            </li>           
                            <li>
                                <a href="javascript:;" data-toggle="collapse" data-target="#Ads_dropdown"><i class="fa fa-fw fa-arrows-v"></i> Advertisements <i class="fa fa-fw fa-caret-down"></i></a>
                                <ul id="Ads_dropdown" class="collapse">
                                    <li>
                                        <a href="AdList.jsp"><i class="fa fa-fw fa-table"></i> List</a>                                
                                    </li>                             
                                    <li>
                                        <a href="AdAdd.jsp"><i class="fa fa-fw fa-table"></i> Add new</a>                                
                                    </li>                             
                                </ul>
                            </li>                     
                            <li>
                                <a href="DashTours.jsp"><i class="fa fa-fw fa-file"></i> Tours</a>
                            </li>
                            <li>
                                <a href="DashPaths.jsp"><i class="fa fa-fw fa-file"></i> Paths</a>
                            </li>    
                            <li>
                                <a href="DashTags.jsp"><i class="fa fa-fw fa-file"></i> Tags</a>
                            </li>  
                            <li>
                                <a href="Home-AtRight.jsp"><i class="fa fa-fw fa-dashboard"></i> RTL Dashboard</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>

                <div id="page-wrapper">

                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    Users
                                </h1>
                                <ol class="breadcrumb">
                                    <li>
                                        <i class="fa fa-dashboard"></i>  <a href="HomeAdmin.jsp">Dashboard</a>
                                    </li>
                                    <li>
                                        <i class="fa fa-arrows-v"></i> Users
                                    </li>
                                    <li class="active">
                                        <i class="fa fa-table"></i> All
                                    </li>

                                </ol>
                            </div>
                        </div>
                        <!-- /.row -->

                        <div class="row">
                            <div class="col-lg-8">
                                <h2>All users</h2>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Point</h3>
                                    </div>                        
                                    <div class="panel-body">                              
                                        <div class="table table-responsive" >
                                            <table id="table-users-all" class="table table-hover table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Online</th>
                                                        <!--<th>User ID</th>-->
                                                        <th>Username</th>
                                                        <th>Location</th>
                                                        <th>Residence</th>
                                                        <th>Actions</th>                                                
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <%
                                                        ArrayList<User> userList = new ArrayList<User>();
                                                        UserService us = new UserService();
                                                        userList = us.getUsersAll();
                                                        for (User u: userList){ 
                                                            session.setAttribute("sessionUser", u);                                                    
                                                            if (u != null)
                                                            {
                                                                String geoL = null;
                                                                String geoR = null;                                                    
                                                                if (u instanceof UserPrivateBasic) 
                                                                {
                                                                    UserPrivateBasic up = (UserPrivateBasic) u;
                                                                    geoL = up.getGeoLast().toString();
                                                                    geoR = up.getGeoResidence().toString();                                                    
                                                                }
                                                                if (u instanceof UserPrivatePremium) 
                                                                {
                                                                    UserPrivatePremium up = (UserPrivatePremium) u;
                                                                    geoL = up.getGeoLast().toString();
                                                                    geoR = up.getGeoResidence().toString();                                                    
                                                                }
                                                                if (u instanceof UserCommercialBasic) 
                                                                {
                                                                    UserCommercialBasic up = (UserCommercialBasic) u;
                                                                    geoR = up.getGeoResidence().toString();                                                    
                                                                }
                                                                if (u instanceof UserCommercialPremium) 
                                                                {
                                                                    UserCommercialPremium up = (UserCommercialPremium) u;
                                                                    geoR = up.getGeoResidence().toString();                                                    
                                                                }
                                                    %>

                                                        <tr>
                                                            <td><%=u.isLoggedIn() %></td>
                                                            <%-- <td><%=u.getUserID() %></td> --%>
                                                            <td><%=u.getUserName()%></td>

                                                            <td><%=geoL%></td>
                                                            <td><%=geoR%></td> 
                                                            <td><a href="ActionServlet?action=deleteUser&pointID=<c:out value='${sessionUser.userID}'/>"><span class="glyphicon glyphicon-trash"></span></a></td>                                                     </tr>                    
                                                    <%      }
                                                        } 
                                                    %>                  
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Online</th>
                                                        <!--<th>User ID</th>-->
                                                        <th>Username</th>
                                                        <th>Location</th>
                                                        <th>Residence</th>
                                                        <th>Actions</th>                                                
                                                    </tr>
                                                </tfoot>                                        
                                            </table>
                                        </div> <!-- class table -->
                                    </div> <!-- panel body -->
                                </div> <!-- panel class -->
                            </div>
                            <!-- Google maps goes here -->
                            <div class="col-md-4">
                                    <h2>Area Map</h2>
                                    <div class="panel panel-default">
                                    <div class="panel-heading">Tour Map</div>
                                    <div class="panel-body">
                                        <div id="simpleMap" style="height:280px;"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- end google maps -->                                  
                        </div>
                        <!-- /.row -->

                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- /#page-wrapper -->

            </div>
            <!-- /#wrapper -->
        </c:when>
        <c:otherwise>
            <div id="wrapper">     
               <div id="page-wrapper">
                    <div class="container-fluid">            
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-info">
                                    <i class="fa fa-info-circle"></i>  <strong><!--You are not authorized to access this content--></strong>
                                </div>
                            </div>
                        </div>            
                    </div>
               </div>
            </div>
        </c:otherwise>
  
    </c:choose>     



    <!-- jQuery -->
    <script src="js/jquery-1.11.1.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- jQuery DataTables-->
    <script src="js/jquery.dataTables-1.10.5.min.js"></script>    
    <script src="js/dataTables.bootstrap.js"></script>   
    
    <!-- Google Maps -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=weather"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCj20qCYRGPtOtNdrjAstzbt3N8-0TOLqg&callback=initMap" type="text/javascript"></script>    
    <script src="assets/js/google-maps/googlemap-conf.js"></script>    
    
    <!-- Landmark Unique JavaScripts -->
    <script src="js/LandmarkScripts.js"></script>    
    
    <script>
    $(document).ready(function(){
        $('#table-users-all').DataTable();
    });     
    </script>    
    
    

</body>

</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="LAEX.Business.User.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<html lang="en">

<head>
    <!-- dealing with timeout -->
    <meta HTTP-EQUIV="refresh" CONTENT="<%= session.getMaxInactiveInterval()%>; URL=LoggedOutOnTimeout.jsp" />

    <link rel="icon" href="favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Landmark Explorer: Home</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/landmark-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
   
   
     

</head>

<body>

    <!-- LJ *********************************************************-->
    <% User usr = (User) (request.getSession().getAttribute("user")); %>
    <!-- LJ *********************************************************-->
    
    <!-- set logged in user's username into localStorage to have it for clearing login status in DB on session timeout -->
    <script type="text/javascript">
        localStorage.setItem('client',<%=usr.getUserName()%>);
    </script>

    <!-- getting value
    <script type="text/javascript">
        some = localStorage.getItem('user');
    </script>    
    -->
    <c:choose>
        <c:when test="${user.type=='COMMERCIAL'}">     
            <div id="wrapper">

                <!-- Navigation -->
                <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="HomePrivate.jsp">LandmarkExplorer</a>

                    </div>
                    <!-- Top Menu Items -->
                    <ul class="nav navbar-right top-nav">
                        <li class="navbar-text">
                            <p>Account Type: <span class="label label-default"><% out.print(usr.getType()); %></span> <span class="label label-default"><% out.print(usr.getPrivilege()); %></span></p>                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>
                                <!-- LJ *********************************************************-->
                                <% out.print(usr.getUserName()); %>
                                <!-- LJ *********************************************************-->
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                                </li>
                                <li class="divider"></li>
                                    <form id="user_menu_logout" method="post" action="ActionServlet">
                                        <li>
                                            <!--
                                        <a href="javascript:;" onclick="document.getElementById('user_menu_logout').submit();"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                                            -->
                                        <a href="javascript:;" onclick="document.getElementById('user_menu_logout').submit();"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                                        <input type="hidden" name="action" value="Logout"/>
                                        <a <% out.print("E"+usr.getUserName()); %>></a> 
                                        </li>
                                    </form>                            
                            </ul>
                        </li>
                    </ul>

                    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <!--<ul class="nav navbar-nav side-nav">-->
                        <ul class="nav navbar-nav side-nav">
                            <li class="active">
                                <a href="HomePrivate.jsp"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                            </li>

                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>

                <div id="page-wrapper">

                    <div class="container-fluid">

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">                        
                                        <div class="cycle-slideshow" data-cycle-center-horz=true data-cycle-center-vert=true>    

                                            <img src="adverts/advert1.jpg" class="img-responsive" alt="advert 1">                                    
                                            <img src="adverts/advert2.png" class="img-responsive" alt="advert 1">                                    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.row -->
                        <div class="row">                
                            <!-- Google maps goes here -->
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">                              
                                        <div id="simpleMap" style="height:500px;"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- end google maps -->                      
                        </div>
                        <!-- /.row -->



                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- /#page-wrapper -->

            </div>
            <!-- /#wrapper -->
        </c:when>
        <c:otherwise>
            <div id="wrapper">     
               <div id="page-wrapper">
                    <div class="container-fluid">            
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-info">
                                    <i class="fa fa-info-circle"></i>  <strong>You are not authorized to access this content</strong>
                                </div>
                            </div>
                        </div>            
                    </div>
               </div>
            </div>
        </c:otherwise>
    </c:choose>       

    <!-- jQuery -->
    <script src="js/jquery-1.11.1.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    
    <!-- Google Maps -->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=weather"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCj20qCYRGPtOtNdrjAstzbt3N8-0TOLqg&callback=initMap" type="text/javascript"></script>    
    <script src="assets/js/google-maps/googlemap-conf.js"></script>    
  
    <!-- include Cycle2 -->
    <script src="js/jquery.cycle2.js"></script>     
    <script src="js/jquery.cycle2.center.js"></script> 


   
   

</body>

</html>

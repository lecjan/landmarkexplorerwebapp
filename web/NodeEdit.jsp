<%@ page language="java" contentType="text/html; charset=EUC-KR" pageEncoding="EUC-KR"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=EUC-KR">
<link type="text/css"
    href="css/jquery-ui-1.8.18.custom.css" rel="stylesheet" />
<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.8.18.custom.min.js"></script>
<title>Add new Place or Edit new one</title>
</head>
<body>
       
    <c:choose>
        <c:when test="${user.type=='ADMINISTRATOR'}">              
        <div id="pointPanelEdit">
            <form id="pointPanelFormEdit" method="post" name="pointPanelFormEdit" action="ActionServlet">
                <fieldset>
                <div class="form-group">
                    <label for="pointId">point ID</label>
                    <input type="text" class="form-control" name="pointID" value="<c:out value='${point.pointID}' />" id="pointID" placeholder="pointID is read only">
                </div>
                </fieldset>  
                <fieldset id="pointPanelFieldSet">                                        
                <div class="form-group">
                    <label for="name">name</label>
                    <input type="text" class="form-control" name="name" value="<c:out value='${point.name}' />" id="name" placeholder="Enter name">
                </div>
                <div class="form-group">
                    <label for="description">description</label>
                    <textarea class="form-control field" rows="2" cols="50" name="description" form="pointPanelFormEdit"  id="description"> <c:out value='${point.description}' /></textarea> 
                </div>

                <div class="form-group">
                    <label for="description">description input</label>
                    <input type="text" class="form-control " name="description" value="<c:out value='${point.description}' />" id="description]" placeholder="Enter description">
                </div>            



                <div class="form-group">
                    <label for="type">type</label>
                    <input type="text" class="form-control" name="type" value="<c:out value='${point.type}' />" id="type" placeholder="Select type">
                </div>
                <div class="form-group">
                    <label for="latitude">geo latitude</label>
                    <input type="text" class="form-control" name="latitude" value="<c:out value='${point.geoLocation.latitude}' />" id="latitude" placeholder="Enter latitude">
                </div>                                        
                <div class="form-group">
                    <label for="longitude">geo longitude</label>
                    <input type="text" class="form-control" name="longitude" value="<c:out value='${point.geoLocation.longitude}' />" id="longitude" placeholder="Enter longitude">
                </div>       
                </fieldset>
                <input type="hidden" name="action" value="updatePoint" id="submit_action"/>
                <input type="submit" value="Submit" />                                         
            </form>        


        </div>                                
        <!-- end pointsPanelEdit -->  
        </c:when>
        <c:otherwise>
            <div id="wrapper">     
               <div id="page-wrapper">
                    <div class="container-fluid">            
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-info">
                                    <i class="fa fa-info-circle"></i>  <strong>You are not authorized to access this content</strong>
                                </div>
                            </div>
                        </div>            
                    </div>
               </div>
            </div>
        </c:otherwise>
    </c:choose>     
        
        
</body>
</html>
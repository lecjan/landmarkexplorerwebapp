<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- TEMPLATE saved from url=(0040)http://getbootstrap.com/examples/signin/ -->
<html lang="en">
    <head>
        <link rel="icon" href="favicon.ico">
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Landmark Explorer</title>

        <!-- Bootstrap core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="css/signin.css" rel="stylesheet">
        <link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">

        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/bootstrap.min.js"></script>

        <script type="text/javascript">
            window.alert = function(){};
            var defaultCSS = document.getElementById('bootstrap-css');
            function changeCSS(css){
                if(css) $('head > link').filter(':first').replaceWith('<link rel="stylesheet" href="'+ css +'" type="text/css" />'); 
                else $('head > link').filter(':first').replaceWith(defaultCSS); 
            }
            $( document ).ready(function() {
              var iframe_height = parseInt($('html').height()); 
            });
        </script>    

    </head>
    <body>
        <div class="container">    
            <h1>Logout failure...</h1>
            <form id="form_link_to_action_servlet" method="post" action="ActionServlet">
                <li>
                <a href="javascript:;" onclick="document.getElementById('form_link_to_action_servlet').submit();">Return to Home Page...</a>
                <input type="hidden" name="action" value="gotoHomePage"/>
                </li>
            </form>             
        </div>
    </body>
</html>
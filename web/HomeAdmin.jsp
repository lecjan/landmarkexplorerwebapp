<%@page import="LAEX.Business.CustomDataType.AccountType"%>
<%@page import="LAEX.Business.User.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html lang="en">

<head>
    <!-- dealing with timeout -->
    <meta HTTP-EQUIV="refresh" CONTENT="<%= session.getMaxInactiveInterval()%>; URL=LoggedOutOnTimeout.jsp" />

    <link rel="icon" href="favicon.ico">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Landmark Explorer: Home</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/landmark-admin.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="css/plugins/morris.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- LJ *********************************************************-->
    <% User usr = (User) (request.getSession().getAttribute("user")); %>
    <!-- LJ *********************************************************-->


<%--
<c:if test="${user.type == 'ADMINISTRATOR'}">
   <p>(A) Current user is: <c:out value="${user.type}"/><p>
</c:if>
<c:if test="${user.type == 'COMMERCIAL'}">
   <p>(C) Current user is: <c:out value="${user.type}"/><p>
</c:if>
<c:if test="${user.type == 'PRIVATE'}">
   <p>(P) Current user is: <c:out value="${user.type}"/><p>
</c:if>       
--%>

    <c:choose>
        <c:when test="${user.type=='ADMINISTRATOR'}">                                             
        <!-- will render only if user is an Administrator -->                                             
            <!-- set logged in user's username into localStorage to have it for clearing login status in DB on session timeout -->
            <script type="text/javascript">
                localStorage.setItem('client',<%=usr.getUserName()%>);
            </script>

            <!-- getting value
            <script type="text/javascript">
                some = localStorage.getItem('user');
            </script>    
            -->

            <div id="wrapper">

                <!-- Navigation -->
                <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="HomeAdmin.jsp">LandmarkExplorer</a>
                    </div>
                    <!-- Top Menu Items -->
                    <ul class="nav navbar-right top-nav">
                        <li class="navbar-text">
                            <p>Account Type: <span class="label label-default"><% out.print(usr.getType()); %></span> <span class="label label-default"><% out.print(usr.getPrivilege()); %></span></p>                        </li>                        
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i>
                                <!-- LJ *********************************************************-->
                                <% out.print(usr.getUserName()); %>
                                <!-- LJ *********************************************************-->
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                                </li>
                                <li class="divider"></li>
                                    <form id="user_menu_logout" method="post" action="ActionServlet">
                                        <li>
                                            <!--
                                        <a href="javascript:;" onclick="document.getElementById('user_menu_logout').submit();"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                                            -->
                                        <a href="javascript:;" onclick="document.getElementById('user_menu_logout').submit();"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                                        <input type="hidden" name="action" value="Logout"/>
                                        <a <% out.print("E"+usr.getUserName()); %>></a> 
                                        </li>
                                    </form>                            
                            </ul>
                        </li>
                    </ul>

                    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                        <!--<ul class="nav navbar-nav side-nav">-->
                        <ul class="nav navbar-nav side-nav">
                            <li class="active">
                                <a href="HomeAdmin.jsp"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-toggle="collapse" data-target="#Users_dropdown"><i class="fa fa-fw fa-arrows-v"></i> Users <i class="fa fa-fw fa-caret-down"></i></a>
                                <ul id="Users_dropdown" class="collapse">
                                    <li>
                                        <a href="UserList.jsp"><i class="fa fa-fw fa-table"></i> List</a>                                
                                    </li>  
                                    <li>
                                        <a href="UserAdd.jsp"><i class="fa fa-fw fa-table"></i> Add new User</a>                                
                                    </li>                               
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:;" data-toggle="collapse" data-target="#Places_dropdown"><i class="fa fa-fw fa-arrows-v"></i> Nodes <i class="fa fa-fw fa-caret-down"></i></a>
                                <ul id="Places_dropdown" class="collapse">
                                    <li>
                                        <a href="NodeList.jsp"><i class="fa fa-fw fa-table"></i> List</a>                                
                                    </li>     
                                    <li>
                                        <a href="NodeEdit.jsp"><i class="fa fa-fw fa-table"></i> Add new Node</a>                                
                                    </li>                               
                                </ul>
                            </li>           
                            <li>
                                <a href="javascript:;" data-toggle="collapse" data-target="#Ads_dropdown"><i class="fa fa-fw fa-arrows-v"></i> Advertisements <i class="fa fa-fw fa-caret-down"></i></a>
                                <ul id="Ads_dropdown" class="collapse">
                                    <li>
                                        <a href="AdList.jsp"><i class="fa fa-fw fa-table"></i> List</a>                                
                                    </li>                             
                                    <li>
                                        <a href="AdAdd.jsp"><i class="fa fa-fw fa-table"></i> Add new Advert</a>                                
                                    </li>                            
                                </ul>
                            </li>                     
                            <li>
                                <a href="DashTours.jsp"><i class="fa fa-fw fa-file"></i> Tours</a>
                            </li>
                            <li>
                                <a href="DashPaths.jsp"><i class="fa fa-fw fa-file"></i> Paths</a>
                            </li>    
                            <li>
                                <a href="DashTags.jsp"><i class="fa fa-fw fa-file"></i> Tags</a>
                            </li>  
                            <li>
                                <a href="Home-AtRight.jsp"><i class="fa fa-fw fa-dashboard"></i> RTL Dashboard</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </nav>

                <div id="page-wrapper">

                    <div class="container-fluid">

                        <!-- Page Heading -->
                        <div class="row">
                            <div class="col-lg-12">
                                <h1 class="page-header">
                                    Dashboard <small>Statistics Overview</small>
                                </h1>
                                <ol class="breadcrumb">
                                    <li class="active">
                                        <i class="fa fa-dashboard"></i> Dashboard
                                    </li>
                                </ol>
                            </div>
                            <!-- Google maps goes here -->
                            <div class="col-md-6">
                                    <h2>Area Map</h2>
                                    <div class="panel panel-default">
                                    <div class="panel-heading">Tour Map</div>
                                    <div class="panel-body">
                                        <div id="simpleMap" style="height:500px;"></div>
                                    </div>
                                </div>
                            </div>
                            <!-- end google maps -->                      
                        </div>

                        <!-- /.row -->


                    </div>
                    <!-- /.container-fluid -->

                </div>
                <!-- /#page-wrapper -->

            </div>
            <!-- /#wrapper -->
            <div id="wrapper">     
               <div id="page-wrapper">
                    <div class="container-fluid">            
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-info">
                                    <i class="fa fa-info-circle"></i>  <strong>You are in administrator dashboard</strong>
                                </div>
                            </div>
                        </div>            
                    </div>
               </div>
            </div>
        </c:when>
        <c:otherwise>
            <div id="wrapper">     
               <div id="page-wrapper">
                    <div class="container-fluid">            
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="alert alert-info">
                                    <i class="fa fa-info-circle"></i>  <strong>You are not authorized to access this content</strong>
                                </div>
                            </div>
                        </div>            
                    </div>
               </div>
            </div>
        </c:otherwise>
    </c:choose>    
        
        <!-- jQuery -->
        <script src="js/jquery-1.11.1.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="js/bootstrap.min.js"></script>

        <!-- Google Maps -->
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=weather"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCj20qCYRGPtOtNdrjAstzbt3N8-0TOLqg&callback=initMap" type="text/javascript"></script>     
        <script src="assets/js/google-maps/googlemap-conf.js"></script>    

    
</body>

</html>

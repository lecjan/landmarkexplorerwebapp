<!-- ver 1.0 -->
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="LAEX.Messages.*"%>

<!-- TEMPLATE saved from url=(0040)http://getbootstrap.com/examples/signin/ -->

<html lang="en">
<head>
    <link rel="icon" href="favicon.ico">
    
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Landmark Explorer Web Application">
    <meta name="author" content="Lech Jankowski">

    <title>Landmark Explorer</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">
    <link href="css/bootstrap.css" rel="stylesheet" id="bootstrap-css">

    <style type="text/css">
    </style>

    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  
    <script type="text/javascript">
        window.alert = function(){};
        var defaultCSS = document.getElementById('bootstrap-css');
        function changeCSS(css){
            if(css) $('head > link').filter(':first').replaceWith('<link rel="stylesheet" href="'+ css +'" type="text/css" />'); 
            else $('head > link').filter(':first').replaceWith(defaultCSS); 
        }
        $( document ).ready(function() {
          var iframe_height = parseInt($('html').height()); 
        });
    </script>    

    
  </head>

  <body>

    <div class="container">   

        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2"> 

            <div class="panel panel-info" style="box-shadow: 0px 0px 40px #555555;">

                <div class="panel-heading">
                    <div style="text-align: center; color: #FFFFFF">
                        <h1><strong>Landmark Explorer</strong></h1>
                    </div>
                    <div class="panel-title">Sign In</div>
                    <div style="float:right; font-size: 80%; position: relative; top:-10px"><a href="#">Forgot password?</a></div>
                </div>     

                <div style="padding-top:30px" class="panel-body" >

                    <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                    <form id="loginform" class="form-horizontal" role="form" method="post" action="ActionServlet">

                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                            <input id="login-username" type="text" class="form-control" name="username" value="" placeholder="username or email" required pattern="[a-zA-Z0-9_-]{6,}" title='minimum 6 characters (letters,numbers,underscore,hyphen)'>                                        
                        </div>

                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                            <input id="login-password" type="password" class="form-control" name="password" placeholder="password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" title='minimum 6 characters (at least one number, one lowercase and one uppercase letter, at least six characters'>
                        </div>

                        <div class="input-group">
                            <div class="checkbox">
                              <label>
                                <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
                              </label>
                            </div>
                        </div>

                        <div style="margin-top:10px" class="form-group">
                            <!-- Button -->
                            <div class="col-sm-12 controls">
                                <button id="btn-login" type="submit" class="btn btn-success">Sign In</button>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12 control">
                                <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                    Don't have an account! 
                                <a href="#" onClick="$('#loginbox').hide(); $('#signupbox').show()">
                                    Sign Up Here
                                </a>
                                </div>
                            </div>
                        </div>    
                        <input type="hidden" name="action" value="Login" id="submit_action"/>
                    </form>     
                </div>                     
            </div>  
            <!-- These is message well -->
            <div class="alert alert-success">
                <% out.println(AppWarningMessage.getText()); %>
            </div>                                      
        </div>
            
        <div id="signupbox" style="display:none; margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                    <div class="panel panel-info" style="box-shadow: 0px 0px 40px #555555;">
                        <div class="panel-heading">
                            <div style="text-align: center; color: #FFFFFF">
                                <h1><strong>Landmark Explorer</strong></h1>
                            </div>                            
                            <div class="panel-title">Sign Up</div>
                            <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="#" onclick="$('#signupbox').hide(); $('#loginbox').show()">Sign In</a></div>
                        </div>  
                        <div class="panel-body" >
                            <form id="signupform" class="form-horizontal" role="form" method="post" action="ActionServlet">
                                
                                <div id="signupalert" style="display:none" class="alert alert-danger">
                                    <p>Error:</p>
                                    <span></span>
                                </div>

                                <div class="form-group">
                                    <label for="register-username" class="col-md-3 control-label">Username</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="register-username" placeholder="User Name" required pattern="[a-zA-Z0-9_-]{6,}" title='minimum 6 characters (letters,numbers,underscore,hyphen)'>
                                    </div>
                                </div>                                
                                
                                <div class="form-group">
                                    <label for="register-email" class="col-md-3 control-label">Email</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="register-email" placeholder="Email Address" required pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" title='Email format is not valid'>
                                    </div>
                                </div>
                                    
                                <div class="form-group">
                                    <label for="register-firstname" class="col-md-3 control-label">First Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="register-firstname" placeholder="First Name" required pattern="^[a-zA-Z' ]{2,50}$" title='Minimum 2 characters (letters, single quote and space)'>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="register-lastname" class="col-md-3 control-label">Last Name</label>
                                    <div class="col-md-9">
                                        <input type="text" class="form-control" name="register-lastname" placeholder="Last Name" required pattern="^[a-zA-Z' ]{2,50}$" title='Minimum 2 characters (letters, single quote and space)'>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="register-password" class="col-md-3 control-label">Password</label>
                                    <div class="col-md-9">
                                        <input type="password" class="form-control" name="register-password" placeholder="Password" required pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}" title='minimum 6 characters (at least one number, one lowercase and one uppercase letter, at least six characters'>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label for="options-account" class="col-md-3 control-label">Account Type</label>                                    
                                    <div class="col-md-9">
                                        <div class="radio">
                                           <label>
                                              <input type="radio" name="options-account" id="optionsAccount1" value="PRIVATE-BASIC" checked> Private Basic (FREE)
                                           </label>
                                        </div>
                                        <div class="radio">
                                           <label>
                                              <input type="radio" name="options-account" id="optionsAccount2" value="PRIVATE-PREMIUM"> Private Premium (&#8364;2.99/m)
                                           </label>
                                        </div>                                
                                        <div class="radio">
                                           <label>
                                              <input type="radio" name="options-account" id="optionsAccount3" value="COMMERCIAL-BASIC"> Commercial Basic (&#8364;9.99/m)
                                           </label>
                                        </div>                                
                                        <div class="radio">
                                           <label>
                                              <input type="radio" name="options-account" id="optionsAccount4" value="COMMERCIAL-PREMIUM"> Commercial Premium (&#8364;19.99/m)
                                           </label>
                                        </div>                                
                                    </div>
                                </div>
                                    
                                <div class="form-group">
                                    <!-- Button -->                                        
                                    <div class="col-md-offset-3 col-md-9">
                                        <button id="btn-signup" type="submit" class="btn btn-info"><i class="icon-hand-right"></i>Sign Up</button>
                                    </div>
                                </div>
                                <input type="hidden" name="action" value="Register" id="submit_action"/>                                
                            </form>
                          
                         </div>
                    </div>

               
               
                
         </div> 
                                    
        
    </div>
                                    
    
<script type="text/javascript">

</script>    


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>
  

</body></html>
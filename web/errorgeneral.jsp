<%@ page isErrorPage="true" language="java" import="java.io.*" contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <!-- Bootstrap Core CSS -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="css/landmark-admin.css" rel="stylesheet">
        <!-- Morris Charts CSS -->
        <link href="css/plugins/morris.css" rel="stylesheet">
        <!-- Custom Fonts -->
        <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">    
        <title>LandmarkExplorer:Error</title>
    </head>
    <body>
        <div id="wrapper">     
           <div id="page-wrapper">
                <div class="container-fluid">            
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-info">
                                <i class="fa fa-info-circle"></i>  <strong>General Error has occured!</strong>Return to previous page or try reloading the page or login into the application...
                            </div>
                        </div>
                    </div>            
                </div>
           </div>
        </div>           
    </body>    
</html>






